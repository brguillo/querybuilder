import querybuilder as qb
from querybuilder.queries.algebra.columns import TableColumn, Placeholder, Value, Constant, One, Zero, Null, True_, False_
from querybuilder.queries.algebra.relations import Table, View
from querybuilder.schemas.constraints import *
from querybuilder.schemas.schemas import Schema, DB

#nodes
ndid = TableColumn(int, "id", parent_fullname="public.node_data_ids", constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()))
node_data_ids = Table("node_data_ids", parent_fullname="public", columns=(ndid,))

ndidr = TableColumn(int, "data_set", parent_fullname="public.node_data", constraints=(ColumnReferences(ndid), ColumnNotNull()))
ndkey = TableColumn(str, "key", parent_fullname="public.node_data", constraints=(ColumnNotNull(),))
ndval = TableColumn(str, "value", parent_fullname="public.node_data", constraints=(ColumnNotNull(),))
node_data = Table("node_data", parent_fullname="public", columns=(ndidr, ndkey, ndval), constraints=(TableUnique(ndidr.tuple_with(ndkey)),))

nid = ndid.buildfrom(ndid, parent_fullname="public.nodes")
nname = TableColumn(str, "name", parent_fullname="public.nodes", constraints=(ColumnUnique(), ColumnNotNull()))
ndata = TableColumn(int, "data", parent_fullname="public.nodes", constraints=(ColumnReferences(ndid),))
nodes = Table("nodes", parent_fullname="public", columns=(nid, nname, ndata))

nodestore = nodes.left_join(node_data_ids, on=nid.eq(ndid)).left_join(node_data, on=ndid.eq(ndidr))

#edges
edid = ndid.buildfrom(ndid, parent_fullname="public.edge_data_ids")
edge_data_ids = Table("edge_data_ids", parent_fullname="public", columns=(edid,))

edidr = TableColumn(int, "data_set", parent_fullname="public.edge_data", constraints=(ColumnReferences(edid), ColumnNotNull()))
edkey = ndkey.buildfrom(ndkey, parent_fullname="public.edge_data")
edval = ndval.buildfrom(ndval, parent_fullname="public.edge_data")
edge_data = Table("edge_data", parent_fullname="public", columns=(edidr, edkey, edval), constraints=(TableUnique(edidr.tuple_with(edkey)),))

eid = edid.buildfrom(edid, parent_fullname="public.edges")
esrc = TableColumn(int, "source", parent_fullname="public.edges", constraints=(ColumnReferences(nid), ColumnNotNull()))
etrgt = esrc.buildfrom(esrc, name="target")
edata = TableColumn(int, "data", parent_fullname="public.edges", constraints=(ColumnReferences(edid),))
edges = Table("edges", parent_fullname="public", columns=(eid, esrc, etrgt, edata), constraints=(TableUnique(esrc.tuple_with(etrgt)), TableCheck(esrc.gt(etrgt))))

edgestore = edges.left_join(edge_data_ids, on=eid.eq(edid)).left_join(edge_data, on=edid.eq(edidr))

#graph
gdid = ndid.buildfrom(ndid, parent_fullname="public.graph_data")
gdkey = TableColumn(str, "key", parent_fullname="public.graph_data", constraints=(ColumnUnique(), ColumnNotNull()))
gdval = ndval.buildfrom(ndval, parent_fullname="public.graph_data")
graph_data = Table("graph_data", parent_fullname="public", columns=(gdid, gdkey, gdval))

graphstore = graph_data

#data
datastore = node_data.select(
    columns=(Constant(str, "node"), *node_data.columns),
    aliases={0: 'origin'}
).union_all(edge_data.select(
    columns=(Constant(str, "edge"), *edge_data.columns)
)).union_all(graph_data.select(
    columns=(Constant(str, "graph"), *graph_data.columns)
))

#schema
public = Schema("public", objects=(graph_data, nodes, node_data, edges, edge_data))

#database
db = DB(schemata=(public,))
