import querybuilder as qb
from querybuilder.queries.algebra.columns import TableColumn, Placeholder, Named, Value, Constant, One, Zero, Null, True_, False_
from querybuilder.queries.algebra.relations import Table, View
from querybuilder.schemas.constraints import *
from querybuilder.schemas.schemas import Schema, DB

#nodes
nid = TableColumn(int, "id", parent_fullname="public.nodes", constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()))
nname = TableColumn(str, "name", parent_fullname="public.nodes", constraints=(ColumnUnique(), ColumnNotNull()))
ncat = TableColumn(str, "category", parent_fullname="public.nodes", constraints=(ColumnCheck(Named(str, "category").inset(set("ABCD"))),))
ncolor = TableColumn(str, "color", parent_fullname="public.nodes", constraints=(ColumnDefault(Constant(str, "black")),))
nshape = TableColumn(str, "shape", parent_fullname="public.nodes", constraints=(ColumnDefault(Constant(str, "black")),))
ngen = TableColumn(int, "generation", parent_fullname="public.nodes")
nodes = Table("nodes", parent_fullname="public", columns=(nid, nname, ncat, ncolor, nshape, ngen))

nodestore = nodes.select(
    columns=(nodes.columns.name, Constant(str, "category"), nodes.columns.category),
    aliases={1: "key", 2: "value"},
).union_all(nodes.select(
    columns=(nodes.columns.name, Constant(str, "color"), nodes.columns.color),
)).union_all(nodes.select(
    columns=(nodes.columns.name, Constant(str, "shape"), nodes.columns.shape),
)).union_all(nodes.select(
    columns=(nodes.columns.name, Constant(str, "generation"), nodes.columns.generation.cast(str)),
)).alias("nodestore")

#edges
eid = nid.buildfrom(nid, parent_fullname="public.edges")
esrc = TableColumn(int, "source", parent_fullname="public.edges", constraints=(ColumnReferences(nid), ColumnNotNull()))
etrgt = esrc.buildfrom(esrc, name="target")
eweight = TableColumn(int, "weight", parent_fullname="public.edges", constraints=(ColumnNotNull(), ColumnDefault(Constant(int, 1))))
ecat = ncat.buildfrom(ncat, parent_fullname="public.edges")
ecolor = ncolor.buildfrom(ncolor, parent_fullname="public.edges")
egen = ngen.buildfrom(ngen, parent_fullname="public.edges")
edges = Table("edges", parent_fullname="public", columns=(eid, esrc, etrgt, eweight, ecat, ecolor, egen), constraints=(TableUnique(esrc.tuple_with(etrgt)), TableCheck(esrc.gt(etrgt))))

edgestore = edges.select(
    columns=(edges.columns.source, edges.columns.target, Constant(str, "weight"), edges.columns.weight.cast(str)),
    aliases={2: "key", 3: "value"},
).union_all(edges.select(
    columns=(edges.columns.source, edges.columns.target, Constant(str, "category"), edges.columns.category),
)).union_all(edges.select(
    columns=(edges.columns.source, edges.columns.target, Constant(str, "color"), edges.columns.color),
)).union_all(edges.select(
    columns=(edges.columns.source, edges.columns.target, Constant(str, "generation"), edges.columns.generation.cast(str)),
)).alias("edgestore")

#graph
gid = nid.buildfrom(nid, parent_fullname="public.graphs")
gname = TableColumn(str, "name", parent_fullname="public.graphs", constraints=(ColumnUnique(), ColumnNotNull(),))
gdescr = TableColumn(str, "description", parent_fullname="public.graphs")
gcolor = TableColumn(str, "color", parent_fullname="public.graphs", constraints=(ColumnDefault(Constant(str, "white")),))
gversion = TableColumn(int, "version", parent_fullname="public.graphs", constraints=(ColumnNotNull(),))
graphs = Table("graphs", parent_fullname="public", columns=(gid, gname, gdescr, gcolor, gversion))

graphstore = graphs.select(
    columns=(Constant(str, "name"), graphs.columns.name),
    where=gid.eq(0),
    aliases={0: "key", 1: "value"},
).union_all(graphs.select(
    columns=(Constant(str, "description"), graphs.columns.description),
)).union_all(graphs.select(
    columns=(Constant(str, "color"), graphs.columns.color),
)).union_all(graphs.select(
    columns=(Constant(str, "version"), graphs.columns.version.cast(str)),
)).alias("graphstore")

#schema
public = Schema("public", objects=(graphs, nodes, edges))

#database
db = DB(schemata=(public,))
