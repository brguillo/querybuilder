import querybuilder as qb
from querybuilder.queries.algebra.columns import TableColumn, Placeholder, Value, Constant, One, Zero, Null, True_, False_, Named
from querybuilder.queries.algebra.relations import Table, View
from querybuilder.schemas.constraints import *
from querybuilder.schemas.schemas import Schema, DB

#nodes
nid = TableColumn(int, "id", parent_fullname="public.nodes", constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()))
nname = TableColumn(str, "name", parent_fullname="public.nodes", constraints=(ColumnUnique(), ColumnNotNull()))
ndid = nid.buildfrom(nid, parent_fullname="public.node_data")
ndnod = TableColumn(int, "node", parent_fullname="public.node_data", constraints=(ColumnReferences(Named(nid.sqltype, nid.name, parent_fullname="nodes")), ColumnNotNull()))
ndkey = TableColumn(str, "key", parent_fullname="public.node_data", constraints=(ColumnNotNull(),))
ndval = TableColumn(str, "value", parent_fullname="public.node_data", constraints=(ColumnNotNull(),))
nodes = Table("nodes", parent_fullname="public", columns=(nid, nname))
node_data = Table("node_data", parent_fullname="public", columns=(ndid, ndnod, ndkey, ndval), constraints=(TableUnique(Named(ndnod.sqltype, ndnod.name).tuple_with(Named(ndkey.sqltype, ndkey.name))),))
nodestore = nodes.left_join(node_data, on=nid.eq(ndnod))

#edges
eid = nid.buildfrom(nid, parent_fullname="public.edges")
esrc = ndnod.buildfrom(ndnod, name="source", parent_fullname="public.edges")
etrgt = esrc.buildfrom(esrc, name="target")
edid = nid.buildfrom(nid, parent_fullname="public.edge_data")
ededg = TableColumn(int, "edge", parent_fullname="public.edge_data", constraints=(ColumnReferences(Named(eid.sqltype, eid.name, parent_fullname="edges")), ColumnNotNull()))
edkey = ndkey.buildfrom(ndkey, parent_fullname="public.edge_data")
edval = ndval.buildfrom(ndval, parent_fullname="public.edge_data")
edges = Table("edges", parent_fullname="public", columns=(eid, esrc, etrgt), constraints=(TableUnique(Named(esrc.sqltype, esrc.name).tuple_with(Named(etrgt.sqltype, etrgt.name))), TableCheck(esrc.gt(etrgt))))
edge_data = Table("edge_data", parent_fullname="public", columns=(edid, ededg, edkey, edval), constraints=(TableUnique(Named(ededg.sqltype, ededg.name).tuple_with(Named(edkey.sqltype, edkey.name))),))
edgestore = edges.left_join(edge_data, on=eid.eq(ededg))

#graph
gdid = nid.buildfrom(nid, parent_fullname="public.graph_data")
gdkey = TableColumn(str, "key", parent_fullname="public.graph_data", constraints=(ColumnUnique(), ColumnNotNull()))
gdval = ndval.buildfrom(ndval, parent_fullname="public.graph_data")
graph_data = Table("graph_data", parent_fullname="public", columns=(gdid, gdkey, gdval))

graphstore = graph_data

#schema
public = Schema("public", objects=(graph_data, nodes, node_data, edges, edge_data))

#database
db = DB(schemata=(public,))
