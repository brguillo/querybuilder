

# Comparison with other Python Querybuilders

+ SQLAlchemy
	+ ORM oriented
		+ binding is made by associating an engine which implicitly sets the dialect
		+ DDL queries from schema object are executed (but can be generated without execution by using the appropriate base class)
		+ schema object manipulation have implication on the DB side (without explicit execution)
	+ rich package, with heavy class structure
	+ join relation are not known as "Selectable" and their own columns do not point to them
	+ metadata == schema ??
+ PyPika
	+ no connection and transaction management
	+ no name resolution based on context
	+ almost no link between schema objects and queries
		+ poor DDL queries
	+ schema object are related from bottom to top only
	+ silent generation of child schema object on `__getattr__`
	
