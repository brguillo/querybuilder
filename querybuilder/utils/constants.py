from __future__ import annotations
from enum import Enum
from typing import Optional


class _MISSING_TYPE(object):
    __slots__ = ()
    _instance: Optional[_MISSING_TYPE] = None

    @staticmethod
    def __new__(cls) -> _MISSING_TYPE:
        if cls._instance is None:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self) -> None:
        if self is not self._instance:
            raise TypeError(
                f"{self.__class__.__qualname__} is supposed to be a singleton class"
            )

    def __hash__(self) -> int:
        cls = self.__class__
        return hash((cls, cls.__qualname__))

    def _repr_pretty(self, printer, cycle=False):
        if cycle:
            printer.text("…")
            return
        printer.text("<MISSING>")


MISSING = _MISSING_TYPE()


class SetCombinator(Enum):
    UNION = "UNION"
    INTERSECT = "INTERSECT"
    EXCEPT = "EXCEPT"

    @classmethod
    def _missing_(cls, value):
        return cls[value.upper()]


class JoinType(Enum):
    CROSS = "CROSS"
    LEFT = "LEFT"
    RIGHT = "RIGHT"
    FULL = "FULL"
    INNER = "INNER"

    @classmethod
    def _missing_(cls, value):
        return cls[value.upper()]
