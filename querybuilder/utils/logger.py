from __future__ import annotations
from collections import deque
import io
import itertools
import sys
from typing import Any, Iterable, Literal, MutableMapping, Optional, Union, overload
import querybuilder.utils.token as qbtoken
import querybuilder.utils.typing as qbtyping
import querybuilder.utils.formatter as qbformatter
import querybuilder.queries.queries as Queriable


class Logger:
    """Query logger

    Parameters
    ----------
    tokenizer: qbtyping.Tokenizer
        the tokenizer to use for tokenizing queries.

    file: _io.TextIOWrapper, default=sys.stdout
        the opened file in which to write the log.

    formatter: Formatter, default=qbformatter.PrettyFormatter
        the formatter to use to format the queries.  Designed for pygments formatters
        (see querybuilder.formatter), it accept any object enjoying a format method.  If
        None, then the query is not formatted, and the `rawformat` result is written by
        the logger.

    active: bool
        whether to log or not.  Inactive logger will not print anything, but will still
        count and/or track (see query_log parameter below) the executed queries.

    bunch_step: int, default=16
        how often to update the log during an execution of a bunch query.

    query_log: type[MutableSequence], default=deque(maxlen=0)
        a MutableSequence concrete factory, for saving, within the logger, all or some
        or none of the last queries (default is to save none of the them).
    """

    __slots__ = (
        "file",
        "tokenizer",
        "formatter",
        "active",
        "query_counter",
        "query_log",
        "bunch_step",
    )

    def __init__(
        self,
        tokenizer: qbtyping.Tokenizer,
        file: io.TextIOWrapper = sys.stdout,
        formatter: qbformatter.Formatter = qbformatter.PrettyFormatter(),
        active: bool = True,
        bunch_step: int = 16,
        query_log: type[MutableSequence[qbqueries.Queriable]] = deque(maxlen=0),
    ):
        self.file = file
        self.tokenizer = tokenizer
        self.formatter = formatter
        self.active = active
        self.query_counter = 0
        self.query_log = query_log
        self.bunch_step = bunch_step

    def print_last_queries(self, stop=None, formatter=None, file=sys.stdout):
        l = len(self.query_log)
        if stop is None:
            stop = l
        for i in range(max(l - stop, 0), l):
            q = self.query_log[i]
            self.log(q, formatter=formatter, file=file)

    @overload
    def __call__(
        self,
        query: qbqueries.Queriable,
        args: Iterable[tuple],
        /,
        many: Literal[True],
        **kwargs,
    ) -> Iterable[tuple]:
        ...

    @overload
    def __call__(
        self, query: qbqueries.Queriable, args: tuple, /, many: Literal[False], **kwargs
    ) -> None:
        ...

    def __call__(
        self,
        query: qbqueries.Queriable,
        args: Union[tuple, Iterable[tuple]] = (),
        /,
        many: bool = False,
        **kwargs,
    ) -> Optional[Iterable[tuple]]:
        self.query_counter += 1
        self.query_log.append(query)
        if self.active and many:
            return self.log_many(query, args, **kwargs)
        elif self.active:
            return self.log(query, args)
        elif many:
            return args

    def log(
        self,
        query: qbqueries.Queriable,
        args: Union[tuple[Any], dict[str, Any]] = (),
        tokenizer: Optional[qbtyping.Tokenizer] = None,
        formatter: Optional[qbformatter.Formatter] = None,
        file: Optional[io.TextIOWrapper] = None,
    ) -> None:
        tokenizer = self.tokenizer if tokenizer is None else tokenizer
        formatter = self.formatter if formatter is None else formatter
        file = self.file if file is None else file

        tokens = query.tokenize(tokenizer)
        if args:
            finaltokens = [
                (qbtoken.Break(level=3, breakline=True), "\n"),
                (qbtoken.Comment.Args, f"  --  ↖{args}"),
            ]
            tokens = itertools.chain(tokens, finaltokens)
        formatted = formatter.format(tokens, file)
        file.write("\n")

    def log_many(
        self,
        query: qbqueries.Queriable,
        I: Iterable[tuple],
        size_hint: Optional[int] = None,
        iterable_of_tuples: bool = False,
        tokenizer: Optional[qbtyping.Tokenizer] = None,
        formatter: Optional[qbformatter.Formatter] = None,
        file: Optional[io.TextIOWrapper] = None,
        bunch_step: Optional[int] = None,
    ) -> Iterable[tuple]:
        # tokenizer = self.tokenizer if tokenizer is None else tokenizer
        formatter = self.formatter if formatter is None else formatter
        file = self.file if file is None else file
        bunch_step = self.bunch_step if bunch_step is None else bunch_step

        self.log(query, tokenizer=tokenizer, formatter=formatter, file=file)
        if file.seekable():
            seek_pos = file.tell()

        if size_hint is None:
            size_hint = "???"
        counter = 0
        prevline_len = 0
        padd_counter = lambda i: str(i).rjust(len(str(size_hint)) + 1, "0")

        def cover_prevline(line, prevline_len):
            curline_len = len(line)
            if curline_len < prevline_len:
                line = line.ljust(prevline_len, " ")
            return line, curline_len

        for args in I:
            if not (counter % bunch_step):
                line = f"  --  ↖:many ({padd_counter(counter)}/~{size_hint}): {args}"
                line, prevline_len = cover_prevline(line, prevline_len)
                argtokens = [(qbtoken.Comment.Args, line)]
                formatter.format(argtokens, file)
                file.flush()
                if file.seekable():
                    file.seek(seek_pos)
                elif file.isatty():
                    file.write("\r")
                else:
                    file.write("\n")
            counter += 1
            yield args

        line = f"  --  ↖:many ({counter}/{counter}): {args}"
        line, prevline_len = cover_prevline(line, prevline_len)
        argtokens = [(qbtoken.Comment.Args, line)]
        formatter.format(argtokens, file)
        file.write("\n")
        file.flush()
