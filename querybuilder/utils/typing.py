from typing import Any, Callable, Iterable, Optional, Protocol, TypeVar
import querybuilder.classes.atoms as qbatoms
import querybuilder.utils.token as qbtoken

__all__ = ["TkString", "TkStream", "NamedProto", "KeyedProto"]

TkString = tuple[qbtoken._TokenType, str]
TkStream = Iterable[TkString]

E = TypeVar("E")
K = TypeVar("K")


class NamedProto(Protocol):
    @property
    def name(self) -> str:
        ...


class KeyedProto(Protocol):
    @property
    def key(self) -> Optional[str]:
        ...


Tokenizer = Callable[[qbatoms.Atom, Any, ...], TkStream]
