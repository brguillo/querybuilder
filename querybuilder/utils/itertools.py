"""Extension of the standard itertools module

This module provides some tool functions dealing with iterables, that
are not present in the standard itertools module.
"""
import collections.abc
from itertools import *


# TODO: looks like (but is different from) more_itertools.intersperse, with n=1
def join(sep, iterable, *, yieldfromsep=False):
    """Join of iterables

    The function is to iterables what str.join is to strings.

    Parameters
    ----------
    sep: object
        the element to insert within iterable to separate successive
        elements.  If the yieldfromsep parameter is True, then sep should be an
        iterable of elements, all being inserted between each two successive
        elements of iterable.

    iterable: iterable
        the iterable in which to insert elements.

    yieldfromsep: bool, default=False
        consider sep as an iterable, and insert each of its elements between
        the elements of iterable.  It is assumed that sep can be reitered
        again and again (pass tuple(sep) as parameter value if this is not
        the case).
    """
    isfirst = True
    for elem in iterable:
        if isfirst:
            isfirst = False
        elif yieldfromsep:
            yield from sep
        else:
            yield sep
        yield elem


def uniq(iterable):
    """Yields elements from iterable without **consecutive** repetitions

    Parameters
    ----------
    iterable: iterable
        the source iterable of elements.
    """
    return iter(map(lambda e: e[0], groupby(iterable)))


def uniq_ever_seen(iterable):
    """Yields elements from iterable without repetitions

    Parameters
    ----------
    iterable: iterable
        the source iterable of elements.  Each element may or may not be
        hashable.

    Note
    ----
    The function memory footprint is of the order of the list containing
    all elements from the output, since every distinct element should be
    saved in some structure (namely, a set if hashable, a list otherwise)
    in order to detect repetitions.
    """
    iterable = list(uniq(iterable))
    hashable_seen = set()
    nonhashable_seen = list()
    for e in iterable:
        try:
            if e not in hashable_seen:
                hashable_seen.add(e)
                yield e
        except TypeError:
            if e not in nonhashable_seen:
                nonhashable_seen.append(e)
                yield e


class Iterator(collections.abc.Iterator):
    """An Iterator class with some length hints

    Parameters
    ----------
    iterable: iterable
        the iterable on which to iter.

    length_min: int, default=0
        a default lower bound on the iterable length.  If iterable enjoys
        the __len__ or the __length_hint__ method, then the parameter is
        ignored, and the result of the method (__len__ having precedence
        over __length_hint__) is taken as lower bound instead.

    length_max: int or None, default=None
        a default upper bound on the iterable length.  A value None is
        interpreted as no known upper bound.  If iterable enjoys the __len__
        or the __length_hint__ method, then the parameter is ignored, and
        the result of the method (__len__ having precedence over
        __length_hint__) is taken as upper bound instead.

    Note
    ----
    If length_min and length_max are equal, then the __length_hint__
    method returns that value.  Otherwise, the method returns the special
    value NotImplemented.
    """

    __slots__ = ("length_min", "length_max", "_iterator")

    def __init__(self, iterable, *, length_min=0, length_max=None):
        if hasattr(iterable, "__len__"):
            length_min = length_max = len(iterable)
        elif hasattr(iterable, "__length_hint__"):
            length_min = length_max = iterable.__length_hint__()
        self._iterator = iter(iterable)
        self.length_min = length_min
        self.length_max = length_max

    def __next__(self):
        res = next(self._iterator)
        self.length_min = max(0, self.length_min - 1)
        self.length_max -= 1
        return res

    def __length_hint__(self):
        if self.length_min == self.length_max:
            return self.length_max
        return NotImplemented

    def __repr__(self):
        return f"{type(self).__name__}({self.iterator}, length_min={self.length_min}, length_max={self.length_max})"
