from __future__ import annotations
import collections
import functools
import importlib
from types import SimpleNamespace
from typing import (
    Any,
    Callable,
    Iterable,
    Literal,
    Mapping,
    Optional,
    Sequence,
    Set,
    Sequence,
    Union,
)
import querybuilder.utils.itertools as qbitertools
import IPython.lib.pretty

# from IPython.lib.pretty import PrettyPrinter

pygments_token_module = None

__all__ = [
    "_TokenKind",
    "_TokenType",
    "Token",
    "Break",
    "Comment",
    "Error",
    "Keyword",
    "Name",
    "Operator",
    "Punctuation",
    "Text",
    "Whitespace",
]


def convert_to_pygments_token(token):
    global pygments_token_module
    if pygments_token_module is None:
        pygments_token_module = importlib.import_module("pygments.token")
    path = []
    parent = pygments_token_module._TokenType()
    for p in token.path:
        path.append(p)
        child = pygments_token_module._TokenType(path)
        child.parent = parent
        parent = child
    return parent


def convert_to_pygments_token2(tokenstrpair, /):
    return convert_to_pygments_token(tokenstrpair[0]), tokenstrpair[1]


class _TokenKind(SimpleNamespace):
    """Tree of token kinds

    Each token kind is identified by its subkind path to the root, namely,
    a tuple of strings that are all accepted by the `_subkind_validator`
    classmethod (by default, valid subkinds are valid identifier starting
    with an uppercase letter).  The tree tree is unique for the class,
    whence two token kinds with equal paths are identical.

    Parameters
    ----------
    name: Optional[str], default=None
        the subkind name.  Only the root is allowed and required to have
        no name, i.e., name `None`.

    parent: Optional[_TokenKind], default=None
        the parent token kind.  Only the root is allowed and required to
        have no parent, i.e., parent `None`.

    Class attributes
    ----------------
    _root: Optional[_TokenKind]
        the root token kind of the unique token kind tree associated with
        the class. Only if the tree has never been initialised, namely, if
        there are no instances of `_TokenKind`, the attribute has value
        `None`.

    Instance attributes
    -------------------
    parent (property): _TokenKind or None
        the parent token kind of the current token kind in the tree, if
        any, or None, if the root.

    name: str
        the name of the current token kind if not the root, `None`
        otherwise.  For any non-root token kinds `tok` of name `nam`, it
        should always be the case that `tok.parent.nam` is `tok`.

    path: tuple[str, ...]
        the tuple of subkinds indicating the path from the root
        to the current token kind in the tree.

    root (property): _TokenKind
        the root token kind of the tree.

    subkinds (property): Iterable[_TokenKind]
        the set of children token kinds in the tree.

    Methods
    -------
    __contains__:
        a token kind contains another if it is one of its ancestors.

    __hash__:
        token kinds are hashable although they are mutable.  Indeed,
        only adding children in seen as a licit mutation, and the
        hash do not take children into account: only the path from
        the root is considered.

    Examples
    --------
    (For doctesting, we initially reset the token kind tree.)
    >>> _TokenKind._root = None

    We first create the root token kind.
    >>> TokenKind = _TokenKind()
    >>> TokenKind
    TokenKind
    >>> _TokenKind._root is TokenKind
    True

    This, as any token kind, is identified by its path (here empty), so
    calling again `_TokenKind()` results in the identical token kind.
    >>> _TokenKind() is TokenKind
    True
    >>> TokenKind.path
    ()

    The default token kind, which is returned when the constructor is
    called without arguments as above, is the root token kind.  It is
    the only token kind which has no name and no parent.
    >>> TokenKind.name
    >>> TokenKind.parent
    >>> _TokenKind(name=None) is TokenKind
    True
    >>> _TokenKind(name=None, parent=None) is TokenKind
    True
    >>> _TokenKind(parent=None) is TokenKind
    True

    We can create subkinds by providing a name and a parent.
    >>> TextK = _TokenKind(name='Text', parent=TokenKind)
    >>> TextK
    TokenKind.Text
    >>> TextK.path
    ('Text',)
    >>> TextK.name
    'Text'
    >>> TextK.parent
    TokenKind
    >>> TextK.parent is TokenKind
    True

    Again, the resulting token kind is unique for its path.
    >>> _TokenKind(name='Text', parent=TokenKind) is TextK
    True
    >>> _TokenKind(name='Text', parent=TokenKind) is TextK
    True

    Subkinds are accessible from their parent as named attributes.
    >>> TokenKind.Text is TextK
    True
    >>> NameK = TextK.Name
    >>> VarK = TextK.Name.Variable
    >>> VarK
    TokenKind.Text.Name.Variable
    >>> VarK.parent
    TokenKind.Text.Name
    >>> VarK.parent.parent
    TokenKind.Text
    >>> VarK.parent.parent is TextK
    True

    The set of already-defined subkinds of a token kind is visible
    through the `subkinds` property:
    >>> sorted(TokenKind.subkinds, key=repr)
    [TokenKind.Text]
    >>> TextK.subkinds
    frozenset({TokenKind.Text.Name})
    >>> NameK.subkinds
    frozenset({TokenKind.Text.Name.Variable})
    >>> VarK.subkinds
    frozenset()

    When accessing an non-existent subkinds of a token kind by named
    attribute, this subkind is created on the fly.
    >>> KeywordK = TokenKind.Keyword
    >>> KeywordK
    TokenKind.Keyword
    >>> KeywordK is TokenKind.Keyword
    True
    >>> KeywordK is _TokenKind(name='Keyword', parent=TokenKind)
    True
    >>> KeywordK.parent is TokenKind
    True
    >>> sorted(TokenKind.subkinds, key=repr)
    [TokenKind.Keyword, TokenKind.Text]

    This allow to create many token kind, if initially undefined, along
    a branch path:
    >>> OBraceK = TokenKind.Punctuation.Delimiter.Brace.Open
    >>> OBraceK
    TokenKind.Punctuation.Delimiter.Brace.Open

    Along this path, all token kinds have (of course) been defined:
    >>> sorted(TokenKind.subkinds, key=repr)
    [TokenKind.Keyword, TokenKind.Punctuation, TokenKind.Text]
    >>> TokenKind.Punctuation.subkinds
    frozenset({TokenKind.Punctuation.Delimiter})
    >>> TokenKind.Punctuation.Delimiter.subkinds
    frozenset({TokenKind.Punctuation.Delimiter.Brace})
    >>> TokenKind.Punctuation.Delimiter.Brace.subkinds
    frozenset({TokenKind.Punctuation.Delimiter.Brace.Open})
    >>> TokenKind.Punctuation.Delimiter.Brace is OBraceK.parent
    True

    Defining a deep token kind is also possible directly, by passing a
    tuple rather than a name (str) to the class constructor.  (This
    feature is mostly implemented for allowing to pass instances of
    `pygments.token._TokenType` (which are particular tuples) to the
    constructor in order to obtain a corresponding token kind).
    >>> CParK = _TokenKind(('Punctuation', 'Delimiter', 'Parenthesis', 'Close'), parent=TokenKind)
    >>> CParK
    TokenKind.Punctuation.Delimiter.Parenthesis.Close
    >>> sorted(TokenKind.Punctuation.Delimiter.subkinds, key=repr)
    [TokenKind.Punctuation.Delimiter.Brace, TokenKind.Punctuation.Delimiter.Parenthesis]
    >>> TokenKind.Punctuation.Delimiter.Parenthesis.subkinds
    frozenset({TokenKind.Punctuation.Delimiter.Parenthesis.Close})
    >>> TokenKind.Punctuation.Delimiter.Parenthesis is CParK.parent
    True
    >>> CParK is _TokenKind(('Punctuation', 'Delimiter', 'Parenthesis', 'Close'), parent=TokenKind)
    True

    An alternative way of accessing or defining tokens is to use the
    `sub` method from the parent kind object.
    >>> sorted(TokenKind.subkinds, key=repr)
    [TokenKind.Keyword, TokenKind.Punctuation, TokenKind.Text]
    >>> CommentK = TokenKind.sub('Comment')
    >>> CommentK
    TokenKind.Comment
    >>> sorted(TokenKind.subkinds, key=repr)
    [TokenKind.Comment, TokenKind.Keyword, TokenKind.Punctuation, TokenKind.Text]

    Just as the class constructor, the `sub` method also accept tuples
    of subkind names as parameters, in place of a simple name.
    >>> CParK is TokenKind.sub(('Punctuation', 'Delimiter', 'Parenthesis', 'Close'))
    True

    To sum up, here are the three standard ways to define as child
    token kind of an already-defined parent token kind:
    >>> _TokenKind(name='Comment', parent=TokenKind) is CommentK
    True
    >>> TokenKind.Comment is CommentK
    True
    >>> TokenKind.sub('Comment') is CommentK
    True

    Finally, the classmethod `fromtree` can be used to create a token kind
    tree similar to a tree-like object.  Such objects might have various
    form.  Notably, they can be Mappings, strings, Iterables, _TokenKind
    instances as well as `pygments.token._TokenType` instances, any object
    instances with the `__dict__` attributes defined, or `None`.  They can
    even be different kind of objects, providing the user gives a way to
    access items (i.e., key/value pairs) from this object (and recursively
    from descendant object it embeds).  Among the above-listed possible
    values which are naturally supported, `None` or an empty string yield
    an empty path while a nonempty string would yield a path of length one
    formed by that string exactly.  Of course, these paths are appended to
    the parent path in both cases.  The use of `None` is mostly aimed to
    indicate deep leaves in a natural way — though an empty iterable would
    lead to an equivalent result.  With an iterable other than a string,
    the elements are taken as leaves whence they should be acceptable
    subkinds (c.f. `_subkind_validator` class method).  The method always
    returns the root token kind.  For instance:
    >>> TokenKind.fromtree(
    ...     {
    ...         'Text': { 'Name': [ 'Variable' ] },
    ...         'Keyword': (),
    ...         'Punctuation': {
    ...             'Delimiter': {
    ...                 'Brace': [ 'Open', 'Close' ],
    ...                 'Parenthesis': [ 'Open', 'Close' ]
    ...             }
    ...          }
    ...     }
    ... )
    TokenKind
    >>> sorted(TokenKind.Punctuation.Delimiter.Brace.subkinds, key=repr)
    [TokenKind.Punctuation.Delimiter.Brace.Close, TokenKind.Punctuation.Delimiter.Brace.Open]

    It is also possible to pass an arbitrary object together with an
    explicit `getitems` parameter which is a callable that, given the
    object (and, recursively, the descendant objects), returns an
    iterable of (key, value) pairs, just as the `Mapping.items`
    standard method, where `key` is interpreted as the subkind, and
    `value` as the tree node child.

    Now that we have seen how to build new token kinds, with an overall
    tree structure, we can investigate the behavior of these objects.

    A token kind is contained in another, if the latter is a possibly
    non-strict ancestor of the former, in the tree.
    >>> VarK in TextK
    True
    >>> VarK in TokenKind
    True
    >>> VarK in VarK
    True
    >>> VarK in TextK.Name.Variable
    True
    >>> TextK in VarK
    False
    >>> KeywordK in TextK
    False
    >>> OBraceK in TextK
    False
    >>> OBraceK in TokenKind.Punctuation
    True

    Token kinds are hashable, although they are mutable.  It is indeed
    possible to add subkinds, as seen before.  However, these subkinds are
    not taken into account in the hash, only the path from the root (which
    is assumed to be immutable) is considered.
    >>> h1 = hash(TextK.Name)
    >>> TextK.Name.subkinds
    frozenset({TokenKind.Text.Name.Variable})
    >>> TextK.Name.Function
    TokenKind.Text.Name.Function
    >>> sorted(TextK.Name.subkinds, key=repr)
    [TokenKind.Text.Name.Function, TokenKind.Text.Name.Variable]
    >>> h2 = hash(TextK.Name)
    >>> h1 == h2
    True

    A subkind name should be valid according to the `_subkind_validator`
    classmethod, which returns `None` if the subkind passed as parameter
    is correct, or raises an exception otherwise.  The default
    implementation, which is based on the `pygments.token._TokenType`'s
    one, allows any identifier whose first char is uppercase.
    >>> TokenKind._subkind_validator('Text')
    >>> TokenKind._subkind_validator('4me')
    Traceback (most recent call last):
        ...
    ValueError: Invalid subkind 4me
    >>> TokenKind._subkind_validator('-me')
    Traceback (most recent call last):
        ...
    ValueError: Invalid subkind -me
    >>> TokenKind._subkind_validator('_underscore')
    Traceback (most recent call last):
        ...
    ValueError: Invalid subkind _underscore

    This classmethod is aimed for internal use, indeed, when creating
    token kinds the constructor checks that the queried subkinds are
    valid.
    >>> _TokenKind(('-Invalid',))
    Traceback (most recent call last):
        ...
    ValueError: Invalid subkind -Invalid
    >>> _TokenKind(('Valid', 'And', 'Then', 'invalid',))
    Traceback (most recent call last):
        ...
    ValueError: Invalid subkind invalid

    Also, when accessing an non-existent attribute of some token kind,
    the classmethod is used in order to know whether a subkind should
    be created or, instead, an `AttributeError` should be raised.
    >>> TokenKind.Undefined
    TokenKind.Undefined
    >>> TokenKind.undefined # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    AttributeError: undefined.
    >>> TokenKind._also_invalid
    Traceback (most recent call last):
        ...
    AttributeError: _also_invalid

    Finally, it is not possible to set attributes of token kinds, even
    when respecting the types and the tree.
    >>> TokenKind.OtherText = TextK
    Traceback (most recent call last):
        ...
    TypeError: Cannot set attribute OtherText of _TokenKind objects
    >>> TokenKind.OtherText = _TokenKind(('OtherText',))
    Traceback (most recent call last):
        ...
    TypeError: Cannot set attribute OtherText of _TokenKind objects
    >>> TokenKind.Text = TextK
    Traceback (most recent call last):
        ...
    TypeError: Cannot set attribute Text of _TokenKind objects

    """

    _root = None
    __slots__ = ("_name", "_parent")

    @classmethod
    def _subkind_validator(cls, subkind: str):
        # inspired from pygments.token._TokenType.__getattr__
        if not (subkind and subkind.isidentifier() and subkind[0].isupper()):
            raise ValueError(f"Invalid subkind {subkind}")

    @classmethod
    def get(
        cls,
        path: tuple[str, ...] = (),
        default: Any = None,
        /,
        *,
        ifmissing: Literal[
            "return_default", "return_deepest", "create", "raise"
        ] = "return_default",
    ) -> Union[_TokenKind, Any]:
        if ifmissing == "raise":
            nexttk = lambda currtk, subkind: currtk.__dict__.get(
                subkind, ValueError(f"No token kind {subkind} in {currtk}")
            )
        elif ifmissing == "return_default":
            nexttk = lambda currtk, subkind: currtk.__dict__.get(subkind, default)
        elif ifmissing == "return_deepest":
            nexttk = lambda currtk, subkind: currtk.__dict__.get(subkind, currtk)
        elif ifmissing == "create":
            nexttk = getattr
        else:
            raise ValueError(f"Invalid value {ifmissing} for 'ifmissing' parameter")
        curr = cls._root
        if curr is None:
            if ifmissing == "raise" or ifmissing == "return_deepest":
                raise ValueError("Token kind tree has not been initialized")
            elif ifmissing == "return_default":
                return default
            elif ifmissing == "create":
                curr = cls()
        for subkind in path:
            prev, curr = curr, nexttk(curr, subkind)
            if isinstance(curr, ValueError):
                raise curr
            elif curr is default and not getattr(default, "parent", None) is prev:
                return default
            elif curr is curr:
                return curr
        return curr

    @property
    def name(self) -> str:
        return self._name

    @property
    def path(self) -> tuple[str, ...]:
        if self.parent:
            return *self.parent.path, self.name
        return ()

    @property
    def parent(self) -> Optional[_TokenKind]:
        return self._parent

    @property
    def root(self) -> Optional[_TokenKind]:
        return self._root

    @property
    def subkinds(self) -> frozenset[_TokenKind]:
        return frozenset(self.__dict__.values())

    @staticmethod
    def __new__(
        cls,
        branch: tuple[str, ...] = (),
        /,
        parent: Optional[_TokenKind] = None,
        name: Optional[str] = None,
        **kwargs,
    ) -> _TokenKind:
        if name is None and branch:
            branch, name = branch[:-1], branch[-1]
        if not parent and (name or branch):
            parent = cls()
        if parent and not name:
            raise ValueError(f"Missing name for token subkind of {parent}")
        elif not name:  # ⇒ not branch and not parent
            self = cls._root
        else:
            cls._subkind_validator(name)
            for p in branch:
                parent = cls(name=p, parent=parent)
            self = parent.__dict__.get(name, None)
        if self is None:
            self = SimpleNamespace.__new__(cls, **kwargs)
            # Initialize only new instances
            self._name = name
            self._parent = parent
            if name:
                parent.__dict__[name] = self
            else:
                cls._root = self
        return self

    def __init__(
        self,
        branch: tuple[str, ...] = (),
        /,
        parent: Optional[_TokenKind] = None,
        name: Optional[str] = None,
        **kwargs,
    ):
        super().__init__(**kwargs)

    @classmethod
    def fromtree(
        cls,
        treelike: Union[_TokenKind, Mapping, Any],
        getitems: Optional[Callable] = None,
        prefix: tuple[str, ...] = (),
    ) -> _TokenKind:
        if getitems:
            items = getitems(treelike)
        elif isinstance(treelike, _TokenKind):
            items = vars(treelike).items()
        elif hasattr(treelike, "subtypes"):
            # for pygments.token._TokenType or _TokenType
            items = ((x, getattr(treelike, x)) for x in treelike.subtypes)
        elif isinstance(treelike, Mapping):
            items = treelike.items()
        elif isinstance(treelike, str):
            items = ((treelike, None),)
        elif isinstance(treelike, Iterable):
            items = ((x, None) for x in treelike)
        elif hasattr(treelike, "__dict__"):
            items = vars(treelike).items()
        elif treelike is None:
            items = ()
        else:
            raise ValueError(
                f"Unable to interpret {treelike} as a tree-like (or leaf) object",
                "Consider using the getitems parameter for specifying how items (i.e., (key, value) pairs) should be retrieved from your object",
            )
        isempty = True
        for k, v in items:
            isempty = False
            cls.fromtree(v, getitems=getitems, prefix=prefix + (k,))
        if isempty:
            cls(prefix)
        return cls.get(prefix, ifmissing="raise")

    def totree(self):
        return {k: v.totree() for k, v in vars(self).items()}

    def sub(self, branch: Union[str, tuple[str, ...]], /, **kwargs) -> _TokenKind:
        cls = self.__class__
        if isinstance(branch, str):
            return cls((branch,), self, **kwargs)
        return cls(branch, self, **kwargs)

    def __contains__(self, other: Any) -> bool:
        return self is other or (
            isinstance(other, self.__class__)
            and other.path[: len(self.path)] == self.path
        )

    def __eq__(self, other: Any) -> bool:
        return self is other

    def __hash__(self) -> int:
        return hash((self.__class__, self.path))

    def __repr__(self) -> str:
        return ".".join(("TokenKind", *self.path))

    def __getattr__(self, attr: str) -> _TokenKind:
        try:
            self._subkind_validator(attr)
        except ValueError:
            raise AttributeError(attr) from None
        res = self.__class__(name=attr, parent=self)
        return res

    def __setattr__(self, attr: str, value: Any):
        if attr in self.__class__.__slots__:
            super().__setattr__(attr, value)
        else:
            raise TypeError(
                f"Cannot set attribute {attr} of {self.__class__.__name__} objects"
            )

    def __dir__(self) -> Iterable[str]:
        superdir = super().__dir__()
        subkinds = self.__dict__
        return *superdir, *subkinds

    def split(self) -> tuple[_TokenKind, ...]:
        if self.parent:
            return (*self.parent.split(), self)
        return (self,)

    to_pygments_token = convert_to_pygments_token


class _TokenType(object):
    """Token

    A Token is an object associated with a _TokenKind.  It inherits most
    of the attributes and methods of `_TokenKind`'s instances.  However,
    many non-identical tokens may share the same (token kind) path, and
    they may possibly carry further parameters, though, by default, they
    do not.  Also, tokens are callable.  Calling a token allows to create
    new instances of tokens sharing same kind.  Token are non-mutable and
    hashable.

    Parameters
    ----------
    kind: _TokenKind, default=_TokenKind()
        the token kind of the required token.


    Examples
    --------
    (For doctesting with the below examples, we first manually reset the
    token kind tree, since it is initialized in the module.)

    With no argument, we get the default token with the default kind.  In
    other words, the following affectations are equivalent.
    >>> Token = _TokenType()
    >>> Token_bis = _TokenType(_TokenKind(()))
    >>> Token == Token_bis and Token_bis == Token
    True

    By contrast to token kinds, each call to the class constructor results
    in a new instance.
    >>> Token is Token_bis
    False

    Still, their respective kinds are identical.
    >>> Token.kind is Token_bis.kind
    True

    Tokens are the true object to manipulate.  They can be manipulated
    just as token kinds, with the tree induced by their kinds:
    >>> Token #the root token
    Token
    >>> Text = Token.Text
    >>> Text
    Token.Text
    >>> Text.parent
    Token

    """

    __slots__ = ("kind",)

    @property
    def subtypes(self):
        return (self.__class__(x) for x in self.kind.subkinds)

    @property
    def path(self):
        return self.kind.path

    def __init__(self, kind: Optional[_TokenKind] = None):
        self.kind = kind or _TokenKind()

    def __contains__(self, other: Any) -> bool:
        return isinstance(other, self.__class__) and other.kind in self.kind

    def __eq__(self, other: Any) -> bool:
        return self.__class__ == other.__class__ and self.kind == other.kind

    def __hash__(self) -> int:
        return hash((self.__class__, self.kind))

    def __repr__(self) -> str:
        return ".".join(("Token", *self.kind.path))

    def __dir__(self) -> Iterable[str]:
        superdir = super().__dir__()
        subtypes = self.kind.subkinds.__dict__
        return *superdir, *subtypes

    def __getattr__(self, attr: str) -> _TokenType:
        res = getattr(self.kind, attr)
        if isinstance(res, _TokenKind):
            return _TokenType(res)
        raise AttributeError(attr)

    def to_pygments_token(self):
        return self.kind.to_pygments_token()

    def __setattr__(self, attr: str, value: Any):
        if any(attr in cls.__slots__ for cls in self.__class__.mro()):
            super().__setattr__(attr, value)
        else:
            raise TypeError("Cannot set attribute: tokens are immutable")

    def __delattr__(self, attr: str):
        raise TypeError("Cannot delete attribute: tokens are immutable")

    def __call__(self, **kwargs) -> _TokenType:
        return self.__class__(self.kind, **kwargs)

    def split(self) -> Sequence[_TokenType]:
        return tuple(map(self.__class__, self.kind.split()))


Token = _TokenType()
Comment = Token.Comment
Error = Token.Error
Keyword = Token.Keyword
Name = Token.Name
Operator = Token.Operator
Punctuation = Token.Punctuation
Text = Token.Text
Whitespace = Text.Whitespace


class _Break(_TokenType):
    """Break Token

    A token that can induce a line break.

    Parameters
    ----------
    level: int, default=0
        Higher level are preferable places for introducing a line break.

    chain: bool, default=False
        Whether introducing a break on this token should imply introducing
        a break on preceding and following Break tokens that have same
        level and which are not separated from this one by a Break of
        higher level.

    indent: int, default=0
        Whether lines following this token should be indented or not (and
        by how much indentation quantity).

    breakline: bool, default=False
        Whether the line break should be forced here, regardless of the
        length of the current line.

    Examples
    --------
    We can get an instance of _Break as follows.
    >>> Break = _Break()
    >>> Break
    Token.Text.Whitespace.Break

    We could have set some or all of the parameters of Break tokens.
    >>> _Break(level=2, indent=1, chain=False) #chain=False is actually the default
    Token.Text.Whitespace.Break(level=2, indent=1)

    Alternatively, we can get new instances from a previous instance:
    >>> Break2 = Break(level=2, chain=True, indent=2)
    >>> Break2
    Token.Text.Whitespace.Break(level=2, chain=True, indent=2)

    Contrary to pygments.token._TokenType objects, the Break instances are
    not identical.
    >>> Break is Break2
    False

    They are not equal, unless they have same parameters.
    >>> Break == Break2 or Break2 == Break
    False
    >>> Break == Break() and Break2 == Break(**Break2.parameters)
    True

    Membership (through `__contains__` dunder) of tokens correspond to
    the ancestor relation in the token tree.¹
    >>> Break in Break2 and Break2 in Break
    True
    >>> Break.ForcedBreak in Break
    True
    >>> Break in Whitespace
    True
    >>> Whitespace in Break
    False

    Break object enjoy a pretty representation, used by IPython pretty
    printing, which indicates in a concise way the values of the various
    parameters when they differ from the default:
    >>> Break2._pretty_repr()
    'Token.Text.Whitespace.Break₂⟶*'
    >>> Break(indent=-1, level=0, chain=False, breakline=True)._pretty_repr()
    'Token.Text.Whitespace.Break₀!←'

    Notes
    -----
    ¹ For the token membership of Break tokens to work correctly with
    pygments tokens, we had to change the `__contains__` of their token
    type (namely, `pygments.token._TokenType.__contains__`), so that it
    allows different types of token, as soon as they are instances of
    their base token class (`_TokenType`).

    """

    parent_token = Whitespace
    token_name = "Break"
    _parameters = dict(level=0, chain=False, indent=0, breakline=False)
    __slots__ = ("parameters",)

    @property
    def level(self):
        return self.parameters["level"]

    @property
    def chain(self):
        return self.parameters["chain"]

    @property
    def indent(self):
        return self.parameters["indent"]

    @property
    def breakline(self):
        return self.parameters["breakline"]

    def __init__(
        self,
        level: int = _parameters["level"],
        chain: bool = bool(_parameters["chain"]),
        indent: int = _parameters["indent"],
        breakline: bool = bool(_parameters["breakline"]),
    ):
        super().__init__(self.parent_token.kind.sub(self.token_name))
        self.parameters = dict(
            level=level, chain=chain, indent=indent, breakline=breakline
        )

    def __call__(
        self,
        level: Optional[int] = None,
        chain: Optional[bool] = None,
        indent: Optional[int] = None,
        breakline: Optional[bool] = None,
    ) -> _Break:
        kwargs = dict(level=level, chain=chain, indent=indent, breakline=breakline)
        kwargs = {k: v for k, v in kwargs.items() if v is not None}
        if not kwargs:
            return self
        kwargs = dict(self.parameters, **kwargs)
        res: _Break = self.__class__(**kwargs)
        return res

    def _repr_pretty_(self, printer: IPython.lib.pretty.PrettyPrinter, cycle: bool):
        printer.text(cycle and "…" or self._pretty_repr())

    def _pretty_repr(self) -> str:
        r = super().__repr__()
        lvl = self.level
        r += "".join(dict(enumerate("₀₁₂₃₄₅₆₇₈₉"))[c] for c in map(int, str(lvl)))
        if self.breakline:
            r += "!"
        if self.indent:
            r += "⟶" if self.indent > 0 else "←"
        if self.chain:
            r += "*"
        return r

    def __repr__(self):
        r = super().__repr__()
        parameters = {
            k: v for k, v in self.parameters.items() if v != self._parameters[k]
        }
        if not parameters:
            return r
        return f"{r}({', '.join(f'{k}={v}' for k, v in parameters.items())})"

    def __contains__(self, other):
        return super().__eq__(other) or (
            other in self.parent and not other == self.parent
        )

    def __eq__(self, other):
        return super().__eq__(other) and self.parameters == other.parameters

    def __hash__(self):
        # Two _Break instances may be unequal and may have different hashes
        return hash(
            (super().__hash__(), self.level, self.chain, self.indent, self.breakline)
        )


Break = _Break()
