"""Module providing formatters

Formatters may appear used at several places in querybuilder.  First, each
dialect is associated with a formatter, which is used to produce pretty
representations of SQL objects.  Second, connector loggers are associated
with two formatters (by default identical) for producing SQL log messages.

A formatter is any object providing a method `format`, which takes an
iterable of tokens and a file as argument (this is the interface fixed by
pygments formatters), and a method `get_formatted` which takes a similar
iterable of tokens only and returns a formatted string.

"""

import collections.abc
import importlib
from io import StringIO
import querybuilder.utils.token as qbtoken
import querybuilder.utils.constants as qbconst

try:
    from pygments.styles.default import DefaultStyle
    from pygments.formatters import Terminal256Formatter

    with_pygments = True
except ModuleNotFoundError:
    with_pygments = False

Break = qbtoken.Break
Whitespace = qbtoken.Whitespace
Token = qbtoken.Token

__all__ = [
    "Formatter",
    "WrapFormatter",
    "PrettyFormatter",
    "StandardFormatter",
    "StandardStyle",
]


class Formatter:
    __slots__ = ("_breakinglines_kwargs",)
    _breakinglines_defaults = dict(
        maxlen=None, indent=0, lineprefix="", initiallineprefix=None
    )

    @property
    def breakinglines_kwargs(self):
        return dict(self._breakinglines_defaults, **self._breakinglines_kwargs)

    @classmethod
    def breakinglines(cls, tokens, **kwargs):
        if "lineprefix" in kwargs:
            kwargs.setdefault("initiallineprefix", None)
        kwargs = dict(cls._breakinglines_defaults, **kwargs)
        maxlen, indent, lineprefix, initiallineprefix = (
            kwargs.pop(key)
            for key in ("maxlen", "indent", "lineprefix", "initiallineprefix")
        )
        if kwargs:
            raise TypeError(
                f"Unexpected parameters {kwargs} in breakinglines class method"
            )
        if initiallineprefix is None:
            initiallineprefix = lineprefix
        if not maxlen:
            if not indent and not initiallineprefix:
                return tokens
            res = []
            if initiallineprefix:
                res.append((Token.OutPrompt, initiallineprefix))
            if indent:
                res.append((Whitespace, indent * indent_base))
            return qbitertools.chain(res, tokens)

        linebreak = "\n"
        indent_base = "\t"
        indent_base_length = len(indent_base)
        tokens = list(tokens)

        linelength = 0
        start_line_indent = indent
        highest_break_level = 0
        last_highest_breaks = []
        chainlevels = set()

        i = 0
        if initiallineprefix:
            tokens.insert(i, (Token.OutPrompt, initiallineprefix))
            i += 1
        tokens.insert(i, (Whitespace, indent * indent_base))
        i += 1
        while i < len(tokens):
            tok, string = tokens[i]
            linelength += len(string)
            # Tracking Breaks
            if tok in Break and linelength:
                indent += tok.indent
                if chainlevels and tok.level >= min(chainlevels):
                    chainlevels = {
                        j
                        for j in chainlevels
                        if j > tok.level or (tok.chain and j == tok.level)
                    }
                    # Force linebreak here
                    tokens[i] = (
                        tok(
                            level=tok.level,
                            chain=tok.chain,
                            indent=tok.indent,
                            breakline=True,
                        ),
                        linebreak,
                    )
                    linelength = 0
                    highest_break_level = 0
                    last_highest_breaks = []
                    i += 1
                    if lineprefix:
                        tokens.insert(i, (Token.OutPrompt, lineprefix))
                        i += 1
                    tokens.insert(i, (Whitespace, indent * indent_base))
                    i += 1
                    continue
                elif tok.breakline:
                    # Force linebreak here
                    tokens[i] = tok, linebreak
                    linelength = 0
                    highest_break_level = 0
                    last_highest_breaks = []
                    i += 1
                    if lineprefix:
                        tokens.insert(i, (Token.OutPrompt, lineprefix))
                        i += 1
                    tokens.insert(i, (Whitespace, indent * indent_base))
                    i += 1
                    continue
                elif tok.level == highest_break_level:
                    last_highest_breaks.append((i, indent))
                elif tok.level > highest_break_level:
                    highest_break_level = tok.level
                    last_highest_breaks = [(i, indent)]
            # Breaking lines (one case of line breaking above, already)
            if (
                last_highest_breaks
                and linelength + start_line_indent * indent_base_length > maxlen
            ):
                i, indent = last_highest_breaks[-1]
                start_line_indent = indent
                tok = tokens[i][0]
                if tok.chain:
                    for j, ind in reversed(last_highest_breaks):
                        jtok = tokens[j][0]
                        if not jtok.chain:
                            break
                        tokens[j] = (
                            jtok(
                                level=jtok.level,
                                chain=jtok.chain,
                                indent=jtok.indent,
                                breakline=True,
                            ),
                            linebreak,
                        )
                        if lineprefix:
                            tokens.insert(j + 1, (Token.OutPrompt, lineprefix))
                            i += 1
                        tokens.insert(j + 2, (Whitespace, ind * indent_base))
                        i += 1
                    chainlevels.add(highest_break_level)
                else:
                    tokens[i] = (
                        tok(
                            level=tok.level,
                            chain=tok.chain,
                            indent=tok.indent,
                            breakline=True,
                        ),
                        linebreak,
                    )
                    if lineprefix:
                        tokens.insert(i + 1, (Token.OutPrompt, lineprefix))
                        i += 1
                    tokens.insert(i + 1, (Whitespace, indent * indent_base))
                    i += 1
                linelength = 0
                highest_break_level = 0
                last_highest_breaks = []
            i += 1
        return tokens

    def __init__(self, **breakinglines_kwargs):
        self._breakinglines_kwargs = breakinglines_kwargs

    def maptokens(self, tokens):
        tokens = self.breakinglines(tokens, **self.breakinglines_kwargs)
        return tokens

    def format(self, tokens, file):
        tokens = self.maptokens(tokens)
        file.write("".join(s[1] for s in tokens))

    def get_formatted(self, tokens):
        with StringIO() as file:
            self.format(tokens, file)
            return file.getvalue()


class WrapFormatter(Formatter):
    __slots__ = ("formatter",)

    def __init__(self, formatter, **breakinglines_kwargs):
        self.formatter = formatter
        super().__init__(**breakinglines_kwargs)

    def format(self, tokens, file):
        tokens = self.maptokens(tokens)
        self.formatter.format(tokens, file)


class StandardStyle(collections.abc.MutableMapping):
    __slots__ = "_styles"

    def __init__(self, E=(), /, **F):
        self._styles = {}
        self.update(E, **F)

    def __getitem__(self, key):
        if isinstance(key, (qbtoken._TokenKind, qbtoken._TokenType)):
            return self[repr(key)]
        res = self._styles.get(key, qbconst.MISSING)
        if res is qbconst.MISSING:
            keysplit = key.split(".")
            for k, v in self._styles.items():
                if key.endswith(k):
                    res = v
                    break
            else:
                for i in range(1, len(keysplit)):
                    keypref = ".".join(keysplit[:i])
                    res = self.get(keypref, qbconst.MISSING)
                    if res is not qbconst.MISSING:
                        break
                else:
                    raise KeyError(key)
        return res

    def __len__(self):
        return len(self._styles)

    def __iter__(self):
        return iter(self._styles)

    def __setitem__(self, key, val):
        if isinstance(key, (qbtoken._TokenKind, qbtoken._TokenType)):
            self[key] = val
        else:
            self._styles[key] = val

    def __delitem__(self, key):
        if isinstance(key, (qbtoken._TokenKind, qbtoken._TokenType)):
            del self[repr(key)]
        else:
            del self._styles[key]


class StandardFormatter(Formatter):
    """A pretty formatter that uses standard libraries only

    Parameters
    ----------
    styles: Any, default=()
        any positional parameter accepted by the style_factory.  With
        the default factory `Style`, it can be a Mapping whose values
        are string transformers (e.g., str.upper) and whose keys are
        strings, _TokenKind, or _TokenType, or it can be an Iterable
        of such key/value pairs.

    **kwargs:
        keyworded parameters that populate either the breakinglines
        default parameter mapping if the key correspond to a key
        expected by the `breakinglines` method, or the local `styles`
        attribute thus completing the `styles` parameter, otherwise.

    Examples
    --------
    Let us consider this simple token stream:
    >>> tokens = [
    ...     (Token.Keyword, "let"),
    ...     (Whitespace, " "),
    ...     (Token.Name, "i"),
    ...     (Token.Operator, "="),
    ...     (Token.Literal, "3"),
    ...     (Whitespace, " "),
    ...     (Token.Keyword, "in"),
    ...     (Whitespace, " "),
    ...     (Token.Name, "i"),
    ...     (Token.Operator, "*"),
    ...     (Token.Literal, "3"),
    ...     (Token.Punctuation, ";"),
    ...     (qbtoken.Break, "\\n")
    ... ]

    We can get a default basic StandardFormatter like this:
    >>> frmt1 = StandardFormatter()

    This formatter can be used to format our token stream.
    >>> frmt1.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    Notice that the keywords 'LET' and 'IN' have been uppercased.  This is
    because the `default_styles` of the standard formatter class defines
    the style for Keyword tokens to be uppercased strings:
    >>> frmt1.default_styles['Keyword']
    <method 'upper' of 'str' objects>
    >>> frmt1.default_styles['Keyword']('bLabLa')
    'BLABLA'

    It is possible to locally change the style as follows:
    >>> frmt1.styles['Keyword'] = str.lower
    >>> frmt1.get_formatted(tokens)
    'let i=3 in i*3;\\n'
    >>> frmt1.styles['Keyword'] = str.upper
    >>> frmt1.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    We could also have specified the wanted style at instanciation time:
    >>> frmt2 = StandardFormatter(Keyword=str.lower)
    >>> frmt2.get_formatted(tokens)
    'let i=3 in i*3;\\n'

    This local changes of styles do not affect the class default styles:
    >>> frmt3 = StandardFormatter()
    >>> frmt3.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    We can change the global default styles as follows:
    >>> StandardFormatter.default_styles['Keyword'] = str.lower
    >>> frmt3.get_formatted(tokens)
    'let i=3 in i*3;\\n'
    >>> frmt2.get_formatted(tokens) #local style is applied
    'let i=3 in i*3;\\n'
    >>> frmt1.get_formatted(tokens) #local style is applied
    'LET i=3 IN i*3;\\n'

    To cancel the definition of a local style, it is enough to delete it:
    >>> del frmt1.styles['Keyword']
    >>> frmt1.get_formatted(tokens)
    'let i=3 in i*3;\\n'

    A style can be any string transformer, namely a callable which takes a
    string as unique argument and returns a string.
    >>> frmt1.styles['Operator'] = " {} ".format
    >>> frmt1.get_formatted(tokens)
    'let i = 3 in i * 3;\\n'

    If a style is not found, style for parent is looked for:
    >>> Whitespace
    Token.Text.Whitespace
    >>> frmt1.styles['Token.Text'] = " ".__add__
    >>> frmt1.get_formatted(tokens)
    'let  i = 3  in  i * 3; \\n'

    """

    __slots__ = ("styles",)
    style_factory = StandardStyle
    default_styles = style_factory(Keyword=str.upper, Name=str.lower)
    default_styling = staticmethod(lambda s: s)

    def __init__(self, styles=(), **kwargs):
        split_kwargs = [{}, {}]
        for k, v in kwargs.items():
            split_kwargs[k in self._breakinglines_defaults][k] = v
        self.styles = self.style_factory(styles, **split_kwargs[0])
        super().__init__(**split_kwargs[1])

    def get_full_styles(self):
        styles = self.style_factory(self.default_styles)
        styles.update(self.styles)
        return styles

    def format(self, tokens, file):
        tokens = self.maptokens(tokens)
        full_styles = self.get_full_styles()
        for tk, s in tokens:
            styling = full_styles.get(tk, self.default_styling)
            file.write(styling(s))


if with_pygments:
    __all__.extend(["PygmentsFormatter", "PygmentsDefaultStyle"])

    class PygmentsDefaultStyle(DefaultStyle):
        default_style = DefaultStyle.styles
        styles = dict(DefaultStyle.styles)
        styles.update(
            {
                qbtoken.Keyword.to_pygments_token(): "bold #3344cc",
                qbtoken.Comment.to_pygments_token(): "italic #999999",
                qbtoken.Name.to_pygments_token(): "#11bb00",
                qbtoken.Name.Function.to_pygments_token(): "#00ff00",
                qbtoken.Punctuation.to_pygments_token(): "#7777cc",
                qbtoken.Operator.to_pygments_token(): "#999999",
                qbtoken.Token.OutPrompt.to_pygments_token(): "#999999",
                qbtoken.Comment.Args.to_pygments_token(): "noitalic #999999",
                qbtoken.Comment.Args.Value.to_pygments_token(): "italic #999999",
                qbtoken.Comment.Args.Value.MISSING.to_pygments_token(): "italic #119999",
            }
        )

    class PygmentsFormatter(WrapFormatter):
        """

        Examples
        --------
        >>> tokens = [
        ...     (Token.Keyword, "let"),
        ...     (Whitespace, " "),
        ...     (Token.Name, "i"),
        ...     (Token.Operator, "="),
        ...     (Token.Literal, "3"),
        ...     (Whitespace, " "),
        ...     (Token.Keyword, "in"),
        ...     (Whitespace, " "),
        ...     (Token.Name, "i"),
        ...     (Token.Operator, "*"),
        ...     (Token.Literal, "3"),
        ...     (Token.Punctuation, ";"),
        ...     (qbtoken.Break, "\\n")
        ... ]
        >>> frmt = PygmentsFormatter()
        >>> formatted = frmt.get_formatted(tokens)
        >>> type(formatted)
        <class 'str'>
        >>> for infix in (ts[1] for ts in tokens):
        ...     assert infix in formatted, (infix, formatted)

        """

        __slots__ = ("formatter_kwargs",)
        subformatter_factory = Terminal256Formatter
        default_subformatter_kwargs = dict(style=PygmentsDefaultStyle)

        @classmethod
        def __issubclass_hook__(cls, other):
            if issubclass(cls.subformatter_factory, other):
                return True
            return NotImplemented

        def __init__(self, **kwargs):
            split_kwargs = [{}, {}]
            for k, v in kwargs.items():
                split_kwargs[k in self._breakinglines_defaults][k] = v
            self.formatter_kwargs = formatter_kwargs = dict(
                self.default_subformatter_kwargs, **split_kwargs[0]
            )
            formatter = self.subformatter_factory(**formatter_kwargs)
            super().__init__(formatter, **split_kwargs[1])

        def maptokens(self, tokens):
            tokens = super().maptokens(tokens)
            tokens = map(qbtoken.convert_to_pygments_token2, tokens)
            return tokens

    PrettyFormatter = PygmentsFormatter
else:
    PrettyFormatter = StandardFormatter

__dir__ = lambda: __all__
