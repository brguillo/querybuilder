from types import MethodType


class instancemethod:
    def __init__(self, callable):
        self.__func__ = callable

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return MethodType(self.__func__, instance)


def typedispatch(func):
    def decored(type, *args, **kwargs):
        for suptype in type.mro():
            if suptype in decored._registry:
                return decored._registry[suptype](type, *args, **kwargs)
        return decored.__func__(type, *args, **kwargs)

    def register(decored, type, func=None):
        def decorator(f):
            decored._registry[type] = f
            return f

        if func is None:
            return decorator
        return decorator(func)

    decored._registry = {}
    decored.__func__ = func
    decored.register = MethodType(register, decored)
    return decored
