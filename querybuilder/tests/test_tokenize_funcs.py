import pytest
import querybuilder as qb
import querybuilder.utils.token as qbtoken
import querybuilder.utils.constants as qbconstants

class TestTokenizeFuncs:
    
    tk = qb.settings["tokenizer"]

    ########
    # name #
    ########

    def test_tokenize_name_empty_str(self):
        expected = [(qbtoken.Name, "")]
        result = list(self.tk.tokenize.name(""))

        assert expected == result

    def test_tokenize_name_simple_str(self):
        expected = [(qbtoken.Name, "foo")]
        result = list(self.tk.tokenize.name("foo"))

        assert expected == result

    def test_tokenize_name_dotted_str(self):
        expected = [(qbtoken.Name, "foo"),
            (qbtoken.Punctuation, "."),
            (qbtoken.Name, "bar")]
        result = list(self.tk.tokenize.name("foo.bar"))

        assert expected == result

    ###########
    # keyword #
    ###########

    def test_tokenize_keyword_one_word_no_spaces(self):
        expected = [(qbtoken.Keyword, "FOO")]
        result = list(self.tk.tokenize.keyword("FOO"))

        assert expected == result

    def test_tokenize_keyword_one_word_end_space(self):
        expected = [
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " ")]
        result = list(self.tk.tokenize.keyword("FOO", endspace=True))

        assert expected == result

    def test_tokenize_keyword_one_word_initial_space(self):
        expected = [
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "FOO")]
        result = list(self.tk.tokenize.keyword("FOO", initialspace=True))

        assert expected == result
    
    def test_tokenize_keyword_one_word_both_spaces(self):
        expected = [
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " ")]
        result = list(self.tk.tokenize.keyword("FOO", initialspace=True, endspace=True))

        assert expected == result

    def test_tokenize_keyword_two_words_no_spaces(self):
        expected = [
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "BAR")]
        result = list(self.tk.tokenize.keyword("FOO BAR"))

        assert expected == result

    def test_tokenize_keyword_two_words_inital_space(self):
        expected = [
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "BAR")]
        result = list(self.tk.tokenize.keyword("FOO BAR", initialspace=True))

        assert expected == result

    def test_tokenize_keyword_two_words_end_space(self):
        expected = [
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "BAR"),
            (qbtoken.Break, " ")]
        result = list(self.tk.tokenize.keyword("FOO BAR", endspace=True))

        assert expected == result

    def test_tokenize_keyword_two_words_both_spaces(self):
        expected = [
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Break, " "),
            (qbtoken.Keyword, "BAR"),
            (qbtoken.Break, " ")]
        result = list(self.tk.tokenize.keyword("FOO BAR", initialspace=True, endspace=True))

        assert expected == result
    
    ############
    # jointype #
    ############

    # TODO: use hypothesis module?

    def test_tokenize_join(self):
        expected = [
            (qbtoken.Keyword, "INNER"),
            (qbtoken.Break(), " "),
            (qbtoken.Keyword, "JOIN")]
        result = list(self.tk.tokenize.jointype(qbconstants.JoinType.INNER, False))

        assert expected == result

    def test_tokenize_natural_join(self):
        expected = [
            (qbtoken.Keyword, "NATURAL"),
            (qbtoken.Break(), " "),
            (qbtoken.Keyword, "FULL"),
            (qbtoken.Break(), " "),
            (qbtoken.Keyword, "JOIN")]
        result = list(self.tk.tokenize.jointype(qbconstants.JoinType.FULL, True))

        assert expected == result


