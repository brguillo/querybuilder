from __future__ import annotations
from functools import singledispatchmethod
from typing import Optional
import querybuilder.classes as classes
from querybuilder.utils.typing import KeyedProto
from querybuilder.utils.constants import MISSING


class WithQueryStore(classes.stores.UNamedStore):
    __slots__ = ()

    @classmethod
    def _prepare(cls, iterable: Iterable[E] = (), /) -> Iterable[E]:
        seen: dict[K, E] = {}

        def fltr(e: E) -> bool:
            k = cls._get_key(e)
            if k is None:
                return True
            prev = seen.get(k)
            if prev is None:
                seen[k] = e
                return True
            elif prev is e:
                return False
            raise ValueError(f"Ambiguous {cls._key_attr} {k!r}, got {prev!r} and {e!r}")

        return filter(fltr, iterable)

    @singledispatchmethod
    def visit(self, obj):
        raise TypeError(f"Unexpected type {obj.__class__}")


class PlaceholderStore(
    classes.stores.KeyedTuple[classes.stores.KeyedProto, Optional[str]]
):
    """KeyedTuple gathering placeholders in order

    Anonymous (i.e., keyless) elements are allowed, as well as key repetitions as far as
    they are associated to equal elements (e.g., two placeholders of distinct sqltype
    should have distinct keys).

    """

    __slots__ = ()

    @staticmethod
    def _get_key(e: KeyedProto, /) -> Optional[str]:
        return e.key

    @singledispatchmethod
    def visit(self, obj):
        raise TypeError(f"Unexpected type {obj.__class__}")

    def to_keyed_map(
        self, autokey: Callable[[int, int], str] = lambda i, j: f"auto{i}"
    ) -> dict[str, "querybuilder.algebra.columns.Placeholder"]:
        """Returns a key-to-Placeholder map

        Unkeyed Placeholder of the store are assigned a key using the autokey parameter
        which should generate a `str` key when called with two integers as parameter:
        the first one being the rank of the unkeyed Placeholder among all the unkeyed
        Placehoders, the second one being its index in the store.
        """
        d = {}
        i = 0
        for j, p in enumerate(self):
            key = self._get_key(p)
            if not key:
                key = autokey(i, j)
                i += 1
            d[key] = p
        return d

    @classmethod
    def to_valued_map(
        cls,
        keyed_map: dict[str, "querybuilder.algebra.columns.Placeholder"],
        *posvals: Any,
        **keyvals: Any,
    ) -> dict[str, Any]:
        """Convert a key-to-Placeholder map into a key-to-Value map"""
        posvals = list(posvals)
        d = {}
        for key, p in keyed_map.items():
            if hasattr(p, "value"):
                d[key] = p.value
            elif key in keyvals:
                d[key] = keyvals[key]
            elif posvals:
                d[key] = posvals.pop(0)
            else:
                d[key] = MISSING
        return d

    def to_value_tuple(self, *posvals: Any, **keyvals: Any) -> tuple[Any]:
        n = len(posvals)
        posvals = list(posvals)
        values = []
        for e in self:
            if e.key in keyvals:
                values.append(keyvals[e.key])
            elif posvals:
                values.append(posvals.pop(0))
            else:
                raise ValueError(
                    f"More than {n} positional placeholder values expected, got {n}"
                )
        if posvals:
            raise ValueError(
                f"{n-len(posvals)} positional placeholder values expected, got {n}"
            )
        return tuple(values)
