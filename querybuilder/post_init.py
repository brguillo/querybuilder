from functools import singledispatchmethod
import querybuilder
from querybuilder.visitors.stores import WithQueryStore, PlaceholderStore


@WithQueryStore.visit.register(querybuilder.classes.atoms.Atom)
@PlaceholderStore.visit.register(querybuilder.classes.atoms.Atom)
def _(self, obj):
    return self


@WithQueryStore.visit.register(querybuilder.queries.algebra.relations.With)
@PlaceholderStore.visit.register(querybuilder.queries.algebra.columns.Placeholder)
def _(self, obj):
    return self | (obj,)


@WithQueryStore.visit.register(querybuilder.queries.dql.WithClosure)
def _(self, obj):
    return self - obj.with_relations


del _
