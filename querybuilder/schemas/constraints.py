from __future__ import annotations
from typing import Iterable, Optional
from enum import Enum

import querybuilder

import querybuilder.classes.atoms as atoms


class Constraint(atoms.Atom):
    __slots__ = ("name", "deferrable", "initially_deferred")

    def __init__(
        self, name: Optional[str] = None, deferrable=None, initially_deferred=None
    ):
        self.name = name
        self.deferrable = deferrable = (
            deferrable if deferrable is not None or initially_deferred is None else True
        )
        self.initially_deferred = None if deferrable is False else initially_deferred

    def _get_subtokenize_kwargs(self, tokenizer):
        sup = super()._get_subtokenize_kwargs(tokenizer)

        defer = ""
        if self.deferrable is None:
            pass
        elif self.deferrable:
            defer = defer + "DEFERRABLE"
            if self.initially_deferred:
                defer = defer + " INITIALLY DEFERRED"
            elif self.initially_deferred is False:
                defer = defer + " INITIALLY IMMEDIATE"
        else:
            defer = defer + "NOT DEFERRABLE"

        sup["defer"] = defer

        if self.name:
            return dict(name=tokenizer.tokenize.name(self.name), **sup)

        return sup


class ColumnConstraint(Constraint):
    __slots__ = ()


class ColumnNotNull(ColumnConstraint):
    __slots__ = ()


class ColumnDefault(ColumnConstraint):
    __slots__ = ("expression",)

    def __init__(self, expression: qbcolumns.Column, **kwargs):
        self.expression = expression
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnCheck(ColumnConstraint):
    __slots__ = ("expression",)

    def __init__(self, expression: qbcolumns.Column, **kwargs):
        # TODO: how to build an immutable table column that has a check constraint involving it?
        self.expression = expression
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnGeneratedAlwaysAs(ColumnConstraint):
    __slots__ = ("expression",)

    def __init__(self, expression: qbcolumns.Expression, **kwargs):
        self.expression = expression
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnGeneratedAsIdentity(ColumnConstraint):
    __slots__ = ("always",)

    def __init__(self, always=True, **kwargs):
        self.always = always
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(always=self.always, **super()._get_subtokenize_kwargs(tokenizer))


class ColumnUnique(ColumnConstraint):
    __slots__ = ()


class ColumnPrimaryKey(ColumnUnique):
    __slots__ = ()


class OnChangePolicy(Enum):
    SET_NULL = "SET NULL"
    SET_DEFAULT = "SET DEFAULT"
    CASCADE = "CASCADE"
    RESTRICT = "RESTRICT"
    NO_ACTION = "NO ACTION"


class ColumnReferences(ColumnConstraint):
    __slots__ = ("refcolumn", "ondelete", "onupdate")
    # TODO: match / deferrable?

    def __init__(
        self,
        refcolumn: TableColumn,
        ondelete: OnChangePolicy = None,
        onupdate: OnChangePolicy = None,
        **kwargs,
    ):
        self.refcolumn = refcolumn
        self.ondelete = ondelete
        self.onupdate = onupdate
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict()

        kwargs["table"] = self.refcolumn.parent_fullname
        kwargs["colname"] = self.refcolumn.name

        OnChangeWrapper = querybuilder.algebra.clauses.OnChangeWrapper
        if self.ondelete:
            kwargs["ondelete"] = OnChangeWrapper("DELETE", self.ondelete).subtokenize(
                tokenizer
            )
        if self.onupdate:
            kwargs["onupdate"] = OnChangeWrapper("UPDATE", self.onupdate).subtokenize(
                tokenizer
            )

        return dict(super()._get_subtokenize_kwargs(tokenizer), **kwargs)


class TableConstraint(Constraint):
    __slots__ = ("expression",)

    def __init__(self, expression: qbcolumns.Expression, **kwargs):
        self.expression = expression
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class TableUnique(TableConstraint):
    __slots__ = ()

    def __init__(self, columns: qbcolumns.Tuple, **kwargs):
        super().__init__(expression=columns, **kwargs)

    # TODO: distinct_nulls ?

    def _get_subtokenize_kwargs(self, tokenizer):
        return super()._get_subtokenize_kwargs(tokenizer)


class TableCheck(TableConstraint):
    __slots__ = ()


class TablePrimaryKey(TableUnique):
    __slots__ = ()


class TableForeignKey(TableConstraint):
    __slots__ = ("references", "ondelete", "onupdate")

    def __init__(
        self,
        columns: qbcolumns.Tuple,
        refcolumns: qbcolumns.Tuple,
        expression: qbcolumns.Tuple,
        ondelete: OnChangePolicy = None,
        onupdate: OnChangePolicy = None,
        **kwargs,
    ):
        self.refcolumns = refcolumns
        self.ondelete = ondelete
        self.onupdate = onupdate
        super().__init__(expression=columns, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            refcolumns=self.refcolumns.subtokenize(tokenizer),
            ondelete=OnChangeWrapper("DELETE", self.ondelete).subtokenize(tokenizer),
            onupdate=OnChangeWrapper("UPDATE", self.onupdate).subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )
