from __future__ import annotations
from typing import Iterable, Optional
import querybuilder.classes.atoms as atoms
import querybuilder.classes.stores as stores
import querybuilder.queries.algebra.relations as relations


class Index(atoms.Atom):
    __slots__ = ("schema_relation", "column", "unique", "name")

    def __init__(
        self, schema_relation, column, name, parent_fullname=None, unique=False
    ):
        self.schema_relation = schema_relation
        self.column = column
        self.unique = unique
        self.name = name
        self.parent_fullname = parent_fullname


class Schema(atoms.Atom):
    __slots__ = (
        "name",
        "_objects",
    )

    def __init__(self, name: str, objects):
        self.name = name
        self._objects = stores.UNamedStore(objects)

    @property
    def relations(self):
        return (c for c in self._objects if isinstance(c, relations.Relation))

    @property
    def tables(self):
        return (c for c in self._objects if isinstance(c, Table))

    @property
    def views(self):
        return (c for c in self._objects if isinstance(c, View))

    @property
    def indexes(self):
        return (c for c in self._objects if isinstance(c, Index))


class DB:
    __slots__ = ("schemata",)

    def __init__(self, schemata: Iterable[Schema]):
        self.schemata = stores.UNamedStore(schemata)
