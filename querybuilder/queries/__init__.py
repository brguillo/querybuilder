from querybuilder.queries.queries import Query
import querybuilder.queries.dcl as dcl
import querybuilder.queries.ddl as ddl
import querybuilder.queries.dml as dml
import querybuilder.queries.dql as dql
import querybuilder.queries.tcl as tcl
import querybuilder.queries.comment as comment
