from typing import ClassVar
from querybuilder.classes.atoms import Atom
import querybuilder


class Query(Atom):
    __slots__ = ()
    _is_readonly: ClassVar[bool]

    def is_readonly(self):
        return self._is_readonly

    def rawformat(self, tokenizer):
        formatter = querybuilder.settings["raw_formatter"]
        tkstream = self.tokenize(tokenizer)
        return formatter.get_formatted(tkstream)

    def with_closure(self, with_relations=None):
        if with_relations is None:
            with_relations = self.get_free_with()
        return querybuilder.queries.dql.WithClosure(self, with_relations)
