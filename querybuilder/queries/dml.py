from querybuilder.queries.queries import Query


class DMLQuery(Query):
    """Base abstract class for DML queries

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.
    """

    __slots__ = ("schema_relation",)
    _is_readonly = False

    def __init__(self, schema_relation, **kwargs):
        self.schema_relation = schema_relation
        super().__init__(**kwargs)

    def accept(self, accumulator):
        accumulator = self.schema_relation.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.schema_relation.descend(accumulator)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            schema_relation=self.schema_relation.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer)
        )


class Insert(DMLQuery):
    """INSERT query class

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    query: qbqueries.dql.Object
        the query from which values to insert are taken.

    in_columns: iterable of schema.Tablecolumn or None, default=None
        a nonempty subset of the schema_relation's columns, in which to
        insert values.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.
    """

    __slots__ = ("in_columns", "query")

    def __init__(self, schema_relation, query, in_columns=None, **kwargs):
        self.query = query
        self.in_columns = in_columns
        super().__init__(schema_relation, **kwargs)

    @property
    def context(self):
        return super().context.add_placeholders(*self.query.context.placeholders)

    def accept(self, accumulator):
        if self.in_columns:
            for c in self.in_columns:
                accumulator = c.accept(accumulator)
        accumulator = self.query.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.in_columns:
            for c in self.in_columns:
                accumulator = c.descend(accumulator)
        accumulator = self.query.descend(accumulator)
        return accumulator

    def _substitute(self, substitutions):
        q = self.query.substitute(substitutions)
        self = self.buildfrom(self, query=q)
        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            query=self.query,
            in_columns=in_columns,
            name=tokenizer.tokenize.name(self.schema_relation.name),
            **super()._get_subtokenize_kwargs(tokenizer)
        )


class Delete(DMLQuery):
    """DELETE query class

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    where: columns.Object or None, default=None
        condition on rows of schema_relation to delete.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.
    """

    __slots__ = ("where",)

    def __init__(self, schema_relation, where=None, **kwargs):
        self.where = where
        super().__init__(schema_relation, **kwargs)

    @property
    def context(self):
        ctxt = super().context
        if self.where:
            ctxt = ctxt.add_placeholders(*self.where.context.placeholders)
        return ctxt

    def accept(self, accumulator):
        if self.where:
            accumulator = self.where.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.where:
            accumulator = self.where.descend(accumulator)
        return accumulator

    def _substitute(self, substitutions):
        where = self.where and self.where.substitute(substitutions)
        self = self.buildfrom(self, where=where)
        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            schema_relation=self.schema_relation.subtokenize(tokenizer),
            where=WhereColumn(self.where).subtokenize(tokenizer),
            unambiguous_name=self.schema_relation.unambiguous_name,
            **super()._get_subtokenize_kwargs(tokenizer)
        )


class Update(DMLQuery):
    """UPDATE query class

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    set_columns: Mapping
        a nonempty Mapping, mapping columns of schema_relation to columns.
        Each key/value pair yields a `key = value` in the SET clause.

    where: columns.Object or None, default=None
        condition on rows of schema_relation to update.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.
    """

    __slots__ = ("where", "set_columns")

    def __init__(self, schema_relation, set_columns, where=None, **kwargs):
        self.set_columns = set_columns
        self.where = where
        super().__init__(schema_relation=schema_relation, **kwargs)

    def accept(self, accumulator):
        if self.where:
            accumulator = self.where.accept(accumulator)

        for cold, cnew in self.set_columns.items():
            accumulator = cold.accept(accumulator) + cnew.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.where:
            accumulator = self.where.descend(accumulator)
        for cold, cnew in self.set_columns.items():
            accumulator = cold.descend(accumulator) + cnew.descend(accumulator)
        return accumulator

    @classmethod
    def _state2hash(cls, state):
        set_columns = state.pop("set_columns")
        set_columns = tuple(set_columns.items())
        state["set_columns"] = set_columns
        return super()._state2hash(state)

    def _substitute(self, substitutions):
        where = self.where and self.where.substitute(substitutions)
        sc = {k: v.substitute(substitutions) for k, v in self.set_columns.items()}
        self = self.buildfrom(self, where=where, set_columns=sc)
        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            schema_relation=self.schema_relation,
            set_columns=self.set_columns,
            where=self.where,
            **super()._get_subtokenize_kwargs(tokenizer)
        )


class Truncate(DMLQuery):
    __slots__ = ()
