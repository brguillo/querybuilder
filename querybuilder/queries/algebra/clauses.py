from __future__ import annotations
from typing import Literal, Optional, Union, Tuple
import querybuilder.utils.constants as qbconstants
import querybuilder.classes.atoms as atoms
import querybuilder.queries.algebra.columns as columns
import querybuilder


class ClauseWrapper(atoms.Atom):
    __slots__ = ()


class ColumnClauseWrapper(ClauseWrapper):
    # This is not a column
    __slots__ = ("column",)

    def __init__(self, column: Union[columns.Column, None]):
        self.column = column

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        if self.column:
            kwargs = dict(column=self.column.subtokenize(tokenizer), **kwargs)
        return kwargs


class DistinctColumn(ColumnClauseWrapper):
    __slots__ = ()

    def __init__(self, arg: Union[columns.Column, Literal[True]]):
        if not arg:
            raise ValueError(arg)
        elif arg is True:
            column = None
        else:
            column = arg
        super().__init__(column=arg)


class AliasedColumn(ColumnClauseWrapper):
    # Not a Column
    __slots__ = ("alias",)

    def __init__(self, column: columns.Column, alias: Optional[str] = None):
        self.alias = alias
        super().__init__(column=column)

    def _get_subtokenize_kwargs(self, tokenizer):
        sup = super()._get_subtokenize_kwargs(tokenizer)
        if self.alias:
            return dict(alias=tokenizer.tokenize.name(self.alias), **sup)
        return sup


class TableColumnWrapper(ColumnClauseWrapper):
    def __init__(self, column: columns.TableColumn):
        self.column = column

    def _get_subtokenize_kwargs(self, tokenizer):
        sup_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        sup_kwargs.pop("column")
        kwargs = {}

        kwargs["constraints"] = [
            c.subtokenize(tokenizer) for c in self.column.constraints
        ]

        kwargs["name"] = self.column.name
        kwargs["sqltype"] = self.column.sqltype

        return dict(kwargs, **sup_kwargs)


class ConstraintNameWrapper(ClauseWrapper):
    __slots__ = "name"

    def __init__(self, name: str = None):
        self.name = name

    def _get_subtokenize_kwargs(self, tokenizer):
        sup_kwargs = super()._get_subtokenize_kwargs(tokenizer)

        if self.name:
            return dict(column=self.column.subtokenize(tokenizer), **sup_kwargs)

        return sup_kwargs


# class ValueTuple(ClauseWrapper):
#    __slots__ = "values"
#
#    def __init__(self, values: Tuple[columns.Constant, ...]):
#        self.values = values
#
#    def _get_subtokenize_kwargs(self, tokenizer):
#        kwargs = super()._get_subtokenize_kwargs(tokenizer)
#
#        kwargs["values"] = [v.subtokenize(tokenizer) for v in self.values]
#
#        return kwargs


class WhereColumn(ColumnClauseWrapper):
    __slots__ = ()


class GroupColumn(ColumnClauseWrapper):
    __slots__ = ()


class HavingColumn(ColumnClauseWrapper):
    __slots__ = ()


class OrderColumn(ColumnClauseWrapper):
    __slots__ = ("how",)

    def __init__(self, column: columns.Column, how: Union[bool, None] = None):
        self.how = how
        super().__init__(column)


class Offset(ClauseWrapper):
    # TODO:
    ...


class Limit(ClauseWrapper):
    # TODO:
    ...


class RelationClauseWrapper(ClauseWrapper):
    # This is not a column
    __slots__ = ("relation",)

    def __init__(self, relation):  #: relations.Fromable):
        self.relation = relation

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs = dict(relation=self.relation.subtokenize(tokenizer), **kwargs)
        return kwargs


class WithClause(ClauseWrapper):
    """A clause that represents the definition of a With query

    Parameters
    ----------

    name: str
        the name of the with query

    query: Query
        the underlying query that is aliased by the With

    materialized: Optional[bool]
        whether the With should be materialized or not.
        If None this value is left blank for the DBMS.

    Examples
    --------

    A WithClause can be obtained seamlessly from a With query:
    >>> from querybuilder.queries.dql import Select
    >>> from querybuilder.queries.algebra.columns import Constant
    >>> s = Select((Constant(int, 1),))
    >>> wc = s.as_with(name="T", materialized=True).to_definition_clause()

    It can then be tokenized as the definition of the with query:
    >>> str(wc)
    'T AS MATERIALIZED (SELECT 1)'

    By default the "MATERIALIZED" field is blank:
    >>> str(s.as_with(name="T2").to_definition_clause())
    'T2 AS (SELECT 1)'
    """

    __slots__ = ("name", "query", "materialized")

    def __init__(self, name: str, query: Query, materialized: Optional[bool] = None):
        self.name = name
        self.query = query
        self.materialized = materialized

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=self.name,
            query=self.query.subtokenize(tokenizer),
            materialized=self.materialized,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class SetCombinatorWrapper(ClauseWrapper):
    __slots__ = ("combinator", "all")

    def __init__(self, combinator: qbconstants.SetCombinator, all: Union[bool, None]):
        self.combinator = combinator
        self.all = all
        super().__init__()

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            combinator=self.combinator,
            all=self.all,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class TemporaryWrapper(
    ClauseWrapper
):  # A wrapper for the TEMPORARY property of some queries
    ...  # Implicitly true


class IfNotExistsWrapper(ClauseWrapper):
    ...  # Implicitly true


class OnChangeWrapper(ClauseWrapper):
    __slots__ = "change", "policy"

    def __init__(self, change, policy):  # TODO: type and import :)
        self.change = change
        self.policy = policy

    def _get_subtokenize_kwargs(self, tokenizer):
        return self._get_slot_values()


class AsQueryWrapper(ClauseWrapper):
    __slots__ = "subquery"

    def __init__(self, subquery):  # TODO: type and import :)
        self.subquery = subquery
        super().__init__()

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            super()._get_subtokenize_kwargs(tokenizer),
            subquery=self.subquery.subtokenize(tokenizer),
        )
