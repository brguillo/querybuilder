from __future__ import annotations
from typing import Tuple
import abc, functools, itertools, operator
from collections import namedtuple
import querybuilder.utils.constants as qbconstants
import querybuilder.classes.atoms as atoms
import querybuilder.classes.stores as stores
import querybuilder.queries.algebra.columns as qbcolumns
import querybuilder.utils.itertools as qbitertools

from typing import Mapping

# import querybuilder.queries.dql as dql
import querybuilder
import querybuilder.queries.algebra.clauses as clauses


# Base classes
class Relation(atoms.Atom):
    """Base class for relations"""

    __slots__ = ("columns",)
    _column_store_factory = stores.NamedStore
    _scopable = False

    def __init__(self, columns=(), **kwargs):
        self.columns = self._column_store_factory(columns)
        super().__init__(**kwargs)

    def accept(self, accumulator):
        for c in self.columns:
            accumulator = c.accept(accumulator)

        return super().accept(accumulator)

    # OVERRIDE
    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for c in self.columns:
            accumulator = c.descend(accumulator)
        return accumulator

    def substitute(self, substitutions):
        """
        Parameters
        ----------
        substitutions: mapping
        """
        if self in substitutions:
            return substitutions[self]
        columns = tuple(
            c.substitute(substitutions, substitute_in_parent=False)
            for c in self.columns
        )
        self = self.buildfrom(self, columns=columns)
        return super().substitute(substitutions)

    def __eq__(self, other):
        if type(self) != type(other) or hash(self) != hash(other):
            return False
        sstate = self.__getstate__()
        ostate = other.__getstate__()
        # in order to avoid infinite recursion, parents of columns (which should self/other) are replaced by parent names
        sstate["columns"] = [
            dict(c.__getstate__(), parent=self.names, hash=hash(c))
            for c in sstate["columns"]
        ]
        ostate["columns"] = [
            dict(c.__getstate__(), parent=self.names, hash=hash(c))
            for c in ostate["columns"]
        ]
        return sstate == ostate

    def __hash__(self):
        return super().__hash__()

    # RELATION SPECIFICS
    @property
    def arity(self):
        return len(self.columns)

    def alias(self, name):
        return Aliased(self, name)


class Fromable(Relation):
    # An example of non-joinable Fromable is "a INNER JOIN b ON a.id=b.fid" c.f., e.g., "c LEFT JOIN a INNER JOIN b ON a.id=b.fid ON c.id=a.fid"
    # No.  Above example works on both postgresql and sqlite3 (parenthesis required around INNER JOIN in sqlite3).
    __slots__ = ()

    def select(self, columns=None, **kwargs):
        """

        Parameters
        ----------
        columns: tuple or None, default=None
            Either a tuple of column specifications or None.  In the latter
            case, all the columns of the relation `self` are used.  Column
            specifications are either indices (int), or unambiguous names
            (str), or columns.  If an index `i`, then the i-th column of
            the subrelation `self` is selected (`self.columns.byindex[i]`).
            If a name `n`, then the corresponding column is selected, if
            unambiguous (`self.columns.byname[n]`).

        Raises
        ------
        IndexError:
            if a column specified by index does not exist.

        KeyError:
            if a column specified by name does not exist.
        """
        if columns is None:
            columns = self.columns
        return querybuilder.queries.dql.Select(from_=self, columns=columns, **kwargs)

    def join(self, jointype, other, on=None):
        """Returned a Join relation

        Parameters
        ----------
        jointype: str or qb.utils.constants.JoinType
            a value for qb.utils.constants.JoinType enumeration
        other: Fromable
            the relation with which to join
        on: column or None
            the column conditioning the join

        Notes
        -----
        Fromable's join_to method is acts as a shorthand to the
        present join method, in which self and other argument
        are exchanged.  E.g., `self.join(jointype, other, col)`
        returns the result of `other.join(jointype, self, col)`.
        """
        jointype = qbconstants.JoinType(jointype)
        return Join(jointype, self, other, on=on)
        # TODO: Why to_relation still here?

    cross_join = functools.partialmethod(join, qbconstants.JoinType.CROSS)
    left_join = functools.partialmethod(join, qbconstants.JoinType.LEFT)
    right_join = functools.partialmethod(join, qbconstants.JoinType.RIGHT)
    full_join = functools.partialmethod(join, qbconstants.JoinType.FULL)
    inner_join = functools.partialmethod(join, qbconstants.JoinType.INNER)

    @functools.wraps(join)
    def join_to(self, jointype, other, on=None):
        return other.join(jointype, self, on=on)

    cross_join_to = functools.partialmethod(join_to, qbconstants.JoinType.CROSS)
    left_join_to = functools.partialmethod(join_to, qbconstants.JoinType.LEFT)
    right_join_to = functools.partialmethod(join_to, qbconstants.JoinType.RIGHT)
    full_join_to = functools.partialmethod(join_to, qbconstants.JoinType.FULL)
    inner_join_to = functools.partialmethod(join_to, qbconstants.JoinType.INNER)


class Named(Fromable):
    """Named relations, which are _fromable_

    Parameters
    ----------
    name : str
        the name of the query
    """

    __slots__ = ("name", "parent_fullname")
    _scopable = False

    def __init__(self, name, parent_fullname=None, **kwargs):
        self.name = name
        self.parent_fullname = parent_fullname
        super().__init__(**kwargs)

    @property
    def fullname(self):
        return ".".join(filter(bool, (self.parent_fullname, self.name)))

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=tokenizer.tokenize.name(self.fullname),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


# Actual classes
class Aliased(Named):
    __slots__: tuple[str, ...] = ("subrelation",)

    def __init__(self, subrelation, name, **kwargs):
        columns = (
            c.buildfrom(c, parent_fullname=name) if hasattr(c, "parent_fullname") else c
            for c in subrelation.columns
        )
        self.subrelation = subrelation
        super().__init__(name, columns=columns, **kwargs)

    def alias(self, name):
        return self.buildfrom(self, name=name)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            subrelation=self.subrelation.subtokenize(tokenizer, scoped=True),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def accept(self, accumulator):
        accumulator = self.subrelation.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        accumulator = self.subrelation.descend(accumulator)
        return accumulator


class With(Named):
    __slots__ = ("subrelation", "materialized")

    def __init__(self, subrelation, name, materialized=None, **kwargs):
        self.subrelation = subrelation
        self.materialized = materialized

        columns = (
            c
            if isinstance(c, qbcolumns.Constant)
            else c.buildfrom(c, parent_fullname=name)
            for c in subrelation.columns
        )
        super().__init__(name, columns=columns)

    def to_definition_clause(self):
        return clauses.WithClause(
            name=self.name, query=self.subrelation, materialized=self.materialized
        )

    def accept(self, accumulator):
        accumulator = self.subrelation.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        accumulator = self.subrelation.descend(accumulator)

        return accumulator


class CartesianProduct(Fromable):
    __slots__ = ("subrelations",)
    _scopable = False

    def __init__(self, subrelations, **kwargs):
        self.subrelations = subrelations
        columns = self._init_columns()
        super().__init__(columns=columns, **kwargs)

    def _init_columns(self):
        return functools.reduce(
            lambda acc, rel: acc + rel.columns,
            self.subrelations,
            self._column_store_factory(),
        )

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            subrelations=tuple(
                r.subtokenize(tokenizer, scoped=True) for r in self.subrelations
            ),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def accept(self, accumulator):
        for r in self.subrelations:
            accumulator = r.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for r in self.subrelations:
            accumulator = r.descend(accumulator)
        return accumulator


class Join(CartesianProduct):
    """Joined relations

    Parameters
    ----------
    jointype: qb.utils.constants.JoinType
        the type of join to perform.

    left: relations.Named
        the relation on the left hand side of the join.

    right: relations.Named
        the relation on the right hand side of the join.

    on: column or None, default=None
        a join condition.

    using: str or None, default=None
        column name on which to join.

    natural: bool, default=False
        whether the join is natural or not.

    **kwargs: mapping
        further keyworded parameters for super initialization.

    """

    __slots__ = ("jointype", "on", "using", "natural")

    def __init__(
        self,
        jointype,
        left: Fromable,
        right: Fromable,
        on=None,
        using: Union[tuple[str], str] = None,
        natural=False,
        **kwargs,
    ):
        oun = sum(map(bool, (on, using, natural)))
        if not oun:
            # TODO: smart behavior there: is it acceptable?  (sqlite3 does the same, not PostgreSQL)
            on = qbcolumns.True_()
        elif oun > 1:
            raise ValueError(
                f"Parameters 'on', 'using', and 'natural' are mutually exclusive"
            )
        if using and not isinstance(using, tuple):
            using = (using,)

        self.jointype = jointype
        self.on = on
        self.using = using
        self.natural = natural
        super().__init__(subrelations=(left, right), **kwargs)

    def _init_columns(self):
        if not self.using and not self.natural:
            return super()._init_columns()
        # In SQL, the output of JOIN … USING and NATURAL JOIN suppresses redundant columns.
        # In the resulting joint relation, these columns are somehow known under several qualified names.
        # Example:
        #   SELECT a.id FROM a INNER JOIN b USING (id);
        #   SELECT b.id FROM a INNER JOIN b USING (id);
        # Here, we consider a.id to be a column of the joint relation, but not b.id.
        columns = self._column_store_factory()
        for rel in self.subrelations:
            for col in rel.columns:
                if self.using and col.name in self.using or self.natural:
                    if col.name in columns._keys:
                        continue
                columns += (col,)
        return columns

    def accept(self, accumulator):
        if self.on:
            accumulator = self.on.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.on:
            accumulator = self.on.descend(accumulator)
        return accumulator

    @property
    def left(self):
        return self.subrelations[0]

    @property
    def right(self):
        return self.subrelations[1]

    def set_on(self, on):
        assert not self.using
        assert not self.natural
        return self.buildfrom(self, on=on)

    def add_on(self, on):
        if self.on:
            return self.set_on(self.on & on)
        assert not self.using
        assert not self.natural
        return self.set_on(on)

    def substitute(self, substitutions):
        on = self.on and self.on.substitute(substitutions)
        self = self.buildfrom(self, on=on)
        return super().substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict(
            jointype=tokenizer.tokenize.jointype(self.jointype, natural=self.natural),
            **super()._get_subtokenize_kwargs(tokenizer),
        )
        if self.on is not None:
            kwargs.update(dict(on=self.on.subtokenize(tokenizer)))
        elif self.using is not None:
            kwargs.update(
                dict(
                    using=tuple(tokenizer.tokenize.name(c) for c in self.using),
                    **kwargs,
                )
            )
        return kwargs


class Table(Named):
    __slots__ = ("constraints",)
    _column_store_factory = stores.UNamedStore

    def __init__(
        self,
        name,
        columns: Iterable[TableColumn],
        parent_fullname: Optional[str] = None,
        constraints: Iterable[TableConstraint] = (),
        **kwargs,
    ):
        self.constraints = stores.UNamedStore(constraints)
        super().__init__(
            name, parent_fullname=parent_fullname, columns=columns, **kwargs
        )

    def create(
        self, if_not_exists=False, as_query=None, temporary=False
    ):  # TODO: type hints
        return querybuilder.queries.ddl.CreateTable(
            self, if_not_exists=if_not_exists, as_query=as_query, temporary=temporary
        )

    def drop(self, *args, **kwargs):
        raise NotImplementedError()

    def alter(self, *args, **kwargs):
        raise NotImplementedError()

    def insert(self, *args, **kwargs):
        raise NotImplementedError()

    def delete(self, *args, **kwargs):
        raise NotImplementedError()

    def update(self, *args, **kwargs):
        raise NotImplementedError()

    def truncate(self, *args, **kwargs):
        raise NotImplementedError()


class View(Named):
    __slots__ = ("aliases", "defquery")

    def __init__(
        self,
        name: str,
        defquery: queries.dql.DQLQuery,
        parent_fullname: Optional[str] = None,
        aliases=(),
        **kwargs,
    ):
        self.defquery = defquery
        self.columns = (
            defquery.columns
        )  # TODO: deal with aliases; recolumn to make self.fullname the parent_fullname of each column
        self.aliases = dict(aliases)
        super().__init__(name, parent_fullname=parent_fullname, **kwargs)

    def __getstate__(self):
        state = super().__getstate__()
        aliases = state.pop("aliases")
        aliases = sorted(aliases.items())
        state["aliases"] = aliases
        return state

    def __setstate__(self, state):
        aliases = state.pop("aliases")
        aliases = dict(aliases)
        state["aliases"] = aliases
        super().__setstate__(state)

    @property
    def column_origins(self):
        return self.defquery.columns

    def create(self, *args, **kwargs):
        raise NotImplementedError()

    def drop(self, *args, **kwargs):
        raise NotImplementedError()

    def alter(self, *args, **kwargs):
        raise NotImplementedError()

    def insert(self, *args, **kwargs):
        raise NotImplementedError()

    def delete(self, *args, **kwargs):
        raise NotImplementedError()

    def update(self, *args, **kwargs):
        raise NotImplementedError()

    def truncate(self, *args, **kwargs):
        raise NotImplementedError()
