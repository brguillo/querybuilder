from __future__ import annotations
import functools
from typing import Union, Iterable
from querybuilder.queries.queries import Query
import querybuilder.queries.algebra.relations as qbrelations
import querybuilder.queries.algebra.columns as qbcolumns
import querybuilder.queries.algebra.clauses as clauses
import querybuilder.utils.constants as qbconstants


class DQLQuery(Query, qbrelations.Relation):
    __slots__ = ()
    _is_readonly = True

    # TODO: add method here to get a "connected relation" (aka Iterable Query)?

    def union(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.UNION, self, other, all=all, **kwargs
        )

    union_all = functools.partialmethod(union, all=True)

    def intersect(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.INTERSECT, self, other, all=all, **kwargs
        )

    intersect_all = functools.partialmethod(intersect, all=True)

    def except_(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.EXCEPT, self, other, all=all, **kwargs
        )

    except_all = functools.partialmethod(except_, all=True)

    def tokenize(self, tokenizer):
        wrs = self.get_free_with()

        if wrs:
            closure = self.with_closure(with_relations=wrs)
            return closure.tokenize(tokenizer)
        else:
            return super().tokenize(tokenizer)


class Values(DQLQuery):
    __slots__ = (
        "values",
        "orderby",
        "limit",
        "offset",
        "limit_with_ties",
        #'fetch'
    )

    def __init__(
        self,
        values: Iterable[Tuple[qbcolumns.Constant, ...]],
        orderby=None,  # TODO: type hints
        limit=None,
        limit_with_ties=None,
        offset=None,
        **kwargs,
    ):
        self.values = tuple(values)
        self.orderby = orderby
        self.limit = limit
        self.limit_with_ties = limit_with_ties
        self.offset = offset
        columns = tuple(
            qbcolumns.Named(sqltype=c.sqltype, name=f"column{i}")
            for i, c in enumerate(self.values[0], start=1)
        )
        super().__init__(columns=columns, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        # kwargs.pop("columns") # we don't need the columns to tokenize the relation
        add_kwargs = {}

        add_kwargs["values"] = tuple(
            qbcolumns.Tuple(*v).subtokenize(tokenizer) for v in self.values
        )

        if self.orderby:
            add_kwargs["orderby"] = clauses.OrderColumn(self.orderby).subtokenize(
                tokenizer
            )
        if self.offset:
            add_kwargs["offset"] = clauses.Offset(self.offset).subtokenize(tokenizer)
        if self.limit:
            add_kwargs["limit"] = clauses.Limit(
                self.limit, limit_with_ties=self.limit_with_ties
            ).subtokenize(tokenizer)
        return dict(**kwargs, **add_kwargs)

    def accept(self, visitor):
        for row in self.values:
            for col in row:
                visitor = col.accept(visitor)

        if self.orderby:
            visitor = self.orderby.accept(visitor)

        return super().accept(visitor)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for row in self.values:
            for col in row:
                accumulator = col.descend(accumulator)
        if self.orderby:
            accumulator = self.orderby.descend(accumulator)
        return accumulator


class Select(DQLQuery):
    __slots__: tuple[str, ...] = (
        "aliases",
        "column_origins",
        "from_",
        "where",
        "groupby",
        "having",
        "orderby",
        "limit",
        "limit_with_ties",
        "offset",
        "distinct",
    )
    _scopable = True

    def __init__(
        self,
        columns,
        distinct=None,
        aliases=(),
        from_=None,
        where=None,
        groupby=None,
        having=None,
        orderby=None,
        limit=False,
        limit_with_ties=None,
        offset=None,
        **kwargs,
    ):
        self.aliases = dict(aliases)
        self.column_origins = tuple(columns)

        def recolumn(i, c):
            if hasattr(c, "name"):
                return qbcolumns.Named(
                    sqltype=c.sqltype, name=self.aliases.get(i, c.name)
                )
            elif i in self.aliases:
                return qbcolumns.Named(sqltype=c.sqltype, name=self.aliases[i])
            else:
                return c

        columns = (recolumn(i, c) for i, c in enumerate(self.column_origins))
        self.distinct = distinct
        self.where = where
        self.groupby = groupby
        self.having = having
        self.orderby = orderby
        self.limit = limit
        self.limit_with_ties = limit_with_ties
        self.offset = offset
        self.from_ = from_

        super().__init__(columns=columns, **kwargs)

    def accept(self, visitor, /):
        for col in self.column_origins:
            visitor = col.accept(visitor)
        for res in (self.from_, self.where, self.groupby, self.having, self.orderby):
            if res:
                visitor = res.accept(visitor)

        return super().accept(visitor)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for ocol in self.column_origins:
            accumulator = ocol.descend(accumulator)

        for res in (self.from_, self.groupby, self.having, self.orderby, self.where):
            if res:
                accumulator = res.descend(accumulator)

        return accumulator

    def set_distinct(self, distinct):
        return self.buildfrom(self, distinct=distinct)

    def set_aliases(self, aliases):
        return self.buildfrom(self, aliases=dict(aliases))

    def update_aliases(self, aliases):
        d = self.aliases.copy()
        d.update(aliases)
        return self.set_aliases(d)

    def reset_aliases(self):
        self.set_aliases(())

    def set_where(self, where):
        return self.buildfrom(self, where=where)

    def set_groupby(self, groupby):
        return self.buildfrom(self, groupby=groupby)

    def set_having(self, having):
        return self.buildfrom(self, having=having)

    def set_order(self, orderby=None, orderwith=None):
        return self.buildfrom(
            self,
            orderby=self.orderby if orderby is None else orderby,
            orderwith=self.orderwith if orderwith is None else orderwith,
        )

    def set_limit(self, limit=None, limit_with_ties=None):
        return self.buildfrom(
            self,
            limit=self.limit if limit is None else limit,
            limit_with_ties=self.limit_with_ties
            if limit_with_ties is None
            else limit_with_ties,
        )

    def set_offset(self, offset):
        return self.buildfrom(self, offset=offset)

    def project(self, indices):
        if isinstance(indices, slice):
            return self.set_columns(self.column_origins.byindex[indices])
        return self.set_columns([self.column_origins.byindex[i] for i in indices])

    def add_where(self, where):
        if self.where:
            return self.set_where(self.where & where)
        return self.set_where(where)

    def add_having(self, having):
        if self.having:
            return self.set_having(self.having & having)
        return self.set_having(having)

    def _substitute(self, substitutions):
        d = {}
        for k in ("where", "distinct", "groupby", "orderby", "having"):
            res = getattr(self, k)
            if hasattr(res, "substitute"):
                res = res.substitute(substitutions)
            d[k] = res
        self = self.buildfrom(self, **d)
        return super()._substitute(substitutions)

    def exists_column(self):
        return columns.Exists(self)

    def exists_query(self):
        return Select((self.exists_column(),))

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        add_kwargs = {}
        add_kwargs["columns"] = tuple(
            clauses.AliasedColumn(c, self.aliases.get(i, None)).subtokenize(tokenizer)
            for i, c in enumerate(self.column_origins)
        )
        if self.distinct:
            add_kwargs["distinct"] = clauses.DistinctColumn(self.distinct).subtokenize(
                tokenizer
            )
        if self.from_:
            add_kwargs["from_"] = clauses.RelationClauseWrapper(self.from_).subtokenize(
                tokenizer
            )
        if self.where:
            add_kwargs["where"] = clauses.WhereColumn(self.where).subtokenize(tokenizer)
        if self.groupby:
            add_kwargs["groupby"] = clauses.GroupColumn(self.groupby).subtokenize(
                tokenizer
            )
            if self.having:
                add_kwargs["having"] = clauses.HavingColumn(self.having).subtokenize(
                    tokenizer
                )
        if self.orderby:
            add_kwargs["orderby"] = clauses.OrderColumn(self.orderby).subtokenize(
                tokenizer
            )
        if self.offset:
            add_kwargs["offset"] = clauses.Offset(self.offset).subtokenize(tokenizer)
        if self.limit:
            add_kwargs["limit"] = clauses.Limit(
                self.limit, limit_with_ties=self.limit_with_ties
            ).subtokenize(tokenizer)

        return dict(**kwargs, **add_kwargs)

    def as_with(self, name, materialized=None):
        return qbrelations.With(self, name=name, materialized=materialized)

    def __getstate__(self):
        state = super().__getstate__()
        aliases = state.pop("aliases", ())
        if aliases:
            aliases = tuple(sorted(aliases.items()))
        state["aliases"] = aliases
        return state

    def __setstate__(self, state):
        aliases = state.pop("aliases")
        aliases = dict(aliases)
        state["aliases"] = aliases
        super().__setstate__(state)


#
#    def tokenize(self, tokenizer):
#        rel = self._collect_with_relations()
#
#        q = self.buildfrom()...
#
#        return super(type(q), q).tokenize(tokenizer)


class SetCombination(DQLQuery):
    """
    Parameters
    ----------
    combinator: SetCombinator
        the Boolean combinator to use: "UNION", "INTERSECT", or "EXCEPT".

    queries: relations.Executable
        the executable relations to combine.

    all: bool or None, default=None
        whether to select rows with multiplicity or distinct rows only.  If None (the
        default), the default SQL behavior (distinct rows only) is used.

    kwargs:
        Additional keyworded parameters for super initialization.
    """

    __slots__ = ("combinator", "all", "subrelations")

    def __init__(
        self,
        combinator: qbconstants.SetCombinator,
        left: DQLQuery,
        right: DQLQuery,
        all: bool = None,
        **kwargs,
    ):
        self.combinator = combinator
        self.all = all
        self.subrelations = (left, right)
        super().__init__(columns=left.columns, **kwargs)

    @property
    def left(self):
        return self.subrelations[0]

    @property
    def right(self):
        return self.subrelations[1]

    def project(self, indices):
        return self.buildfrom(
            self, subrelations=(self.left.project(indices), self.right.project(indices))
        )

    def set_aliases(self, aliases):
        return self.buildfrom(
            self, subrelations=(self.left.set_aliases(aliases), self.right)
        )

    def __getstate__(self):
        state = super().__getstate__()
        state.pop("columns")
        return state

    def __setstate__(self, state):
        state.setdefault("columns", state["subrelations"][0].columns)
        super().__setstate__(state)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            subrelations=(
                self.left.subtokenize(tokenizer),
                self.right.subtokenize(tokenizer),
            ),
            combinator=clauses.SetCombinatorWrapper(
                self.combinator, all=self.all
            ).subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class WithClosure(DQLQuery):
    """Represents the closure of a DQL query with some with relations.

    Parameters
    ----------

    query: DQLQuery
       the query that is closed

    with_relations: tuple[With] or With
        the With relation(s) that should be defined in this closure

    Examples
    --------

    >>> from querybuilder.queries.algebra.columns import Constant
    >>> w1 = Select((Constant(int, 1),)).as_with(name="T", materialized=True)
    >>> w2 = Select((Constant(str, "a"),)).as_with(name="T2", materialized=False)
    >>> j = w1.full_join(w2).select()
    >>> str(j.with_closure((w1, w2)))
    "WITH T AS MATERIALIZED (SELECT 1), T2 AS NOT MATERIALIZED (SELECT 'a') SELECT 1, 'a' FROM T FULL JOIN T2 ON True"
    """

    __slots__ = ("query", "with_relations")

    def __init__(
        self, query: DQLQuery, with_relations: Union[tuple[With], With] = None
    ):
        self.query = query

        if with_relations and not isinstance(with_relations, tuple):
            with_relations = (with_relations,)

        self.with_relations = with_relations

        super().__init__(columns=self.query.columns)

    def accept(self, visitor):
        visitor = self.query.accept(visitor)

        for w in self.with_relations:
            visitor = w.accept(visitor)

        return super().accept(visitor)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)

        accumulator = self.query.descend(accumulator)

        for w in self.with_relations:
            accumulator = w.descend(accumulator)

        return accumulator

    def with_closure(self, with_relations=None):
        if with_relations is None:  # TODO: scrap this feature?
            with_relations = self.get_free_with()

        return self.buildfrom(self, with_relations=self.with_relations + with_relations)

    def _get_subtokenize_kwargs(self, tokenizer):
        d = super()._get_subtokenize_kwargs(tokenizer)
        return dict(
            **d,
            with_relations=tuple(
                wq.to_definition_clause().subtokenize(tokenizer)
                for wq in self.with_relations
            ),
            query=self.query.subtokenize(tokenizer),
        )

    def alias(self, name):
        # TODO: protocol or inheritance?
        return qbrelations.Aliased(self, name)
