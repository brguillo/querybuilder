from typing import Iterable
from querybuilder.queries.queries import Query
import querybuilder.queries.algebra.clauses as qbclauses


class DDLQuery(Query):
    __slots__ = ("schema_relation",)
    _is_readonly = False

    def __init__(self, schema_relation, **kwargs):
        self.schema_relation = schema_relation
        super().__init__(**kwargs)

    def accept(self, accumulator):
        accumulator = self.schema_relation.accept(accumulator)

        return super().accept(accumulator)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.schema_relation.descend(accumulator)


class CreateTable(DDLQuery):
    __slots__ = ("if_not_exists", "as_query", "temporary")

    def __init__(
        self,
        schema_relation,
        if_not_exists=False,
        as_query=None,
        temporary=False,
        **kwargs
    ):
        self.if_not_exists = if_not_exists
        self.as_query = as_query
        self.temporary = temporary
        super().__init__(schema_relation=schema_relation, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        add_kwargs = {}

        if self.temporary:
            add_kwargs["temporary"] = qbclauses.TemporaryWrapper().subtokenize(
                tokenizer
            )

        if self.if_not_exists:
            add_kwargs["if_not_exists"] = qbclauses.IfNotExistsWrapper().subtokenize(
                tokenizer
            )

        add_kwargs["name"] = self.schema_relation.subtokenize(tokenizer)

        if self.as_query:
            add_kwargs["as_query"] = qbclauses.AsQueryWrapper(
                self.as_query
            ).subtokenize(tokenizer)
            # TODO: Does CREATE TABLE AS QUERY accept column aliasing?
        else:
            add_kwargs["columns"] = tuple(
                qbclauses.TableColumnWrapper(c).subtokenize(tokenizer)
                for c in self.schema_relation.columns
            )

        # TODO: Does CREATE TABLE AS QUERY accept constraints?
        add_kwargs["constraints"] = tuple(
            c.subtokenize(tokenizer) for c in self.schema_relation.constraints
        )

        return dict(**kwargs, **add_kwargs)
