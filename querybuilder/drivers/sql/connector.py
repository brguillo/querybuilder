from typing import ClassVar
import querybuilder


class Connector:
    """Base abstract class for connectors

    These connectors are wrappers of any connector implementing
    the Python Database API (PEP 249)¹.  They provide a way of
    executing queries given by mean of querybuilder queries
    object rather than strings.  Additionally, they will own
    an internal transaction manager and, possibly, a logger.

    Parameters
    ----------
    connect_info : any
        A mandatory parameter expected by the backend connector,
        to establish the connection.

    logger : bool or Mapping or querybuilder.utils.logger.Logger or None, default=None
        A logger specification.  A Boolean value will result in
        the default logger, initially activated or not according
        to this Boolean value.  A Mapping value will be passed
        as set of keyworded parameters to the default logger
        constructor.  A Logger will be kept unchanged.  Finally,
        the None value (default) will indicate that no logger
        should be use.  Alternatively, the logger might be set
        after initialization, using the set_logger method which
        accepts as unique argument any of these specifications.
        Logger may include an history manager (this is the case
        for the default logger).

    kwargs : Mapping
        Further keyworded parameters for super initialization.

    Abstract methods
    ----------------
    connect
        This parameterless method should establish the backend
        connection, and refers it as 'session' attribute.

    References
    ----------
    ¹ https://peps.python.org/pep-0249/)
    """

    __slots__ = (
        "_connect_info",
        "session",
        "_logger",
        "logger",
        "transaction",
        "tokenizer",
    )

    OperationalError: ClassVar[Exception]

    def __init__(self, connect_info, logger=None, tokenizer=None):
        self._set_tokenizer(tokenizer)
        self._set_connect_info(connect_info)
        self.set_logger(logger)
        self._set_transaction()
        self.connect()

    def _set_tokenizer(self, tokenizer=None):
        if tokenizer is None:
            tokenizer = querybuilder.drivers.sql.tokenizer.Tokenizer()
        self.tokenizer = tokenizer

    def set_logger(self, logger):
        self._logger = logger
        if isinstance(logger, bool):
            self.logger = querybuilder.utils.logger.Logger(
                active=logger, tokenizer=self.tokenizer
            )
        elif hasattr(logger, "keys"):
            self.logger = querybuilder.utils.logger.Logger(**logger)
        else:
            self.logger = logger

    def _set_transaction(self):
        self.transaction = querybuilder.utils.transaction_manager.TransactionManager(
            self
        )

    def _set_connect_info(self, connect_info):
        self._connect_info = connect_info

    def connect(self):
        raise NotImplementedError()

    def close(self):
        if self.session:
            self.session.close()

    def __del__(self):
        self.close()

    def execute(self, query, args=()):
        with self.transaction(on_next_context=not query.is_readonly()) as t:
            return t.execute(query, args=args)

    def executemany(self, query, I=(), size_hint=None, key_order=None, **kwargs):
        with self.transaction as t:
            return t.executemany(
                query, I, size_hint=size_hint, key_order=key_order, **kwargs
            )

    def cursor(self):
        return self.session.cursor()

    def __hash__(self):
        raise TypeError(f"unhashable type: {type(self)}")

    def get_placeholder_marker(cls, key, *, paramstyle="qmark"):
        """produce the placeholder marker

        Parameters
        ----------
        key : str or int or None
            the assumed key of the Placeholder column.  The accepted
            type depends on the value of the paramstyle parameter.

        paramstyle : str
            the parameter stating the type of marker formatting
            expected by the DB connector interface.  See details on
            https://peps.python.org/pep-0249/#paramstyle .  Methods
            overriding this class method are not expected to propose
            this parameter.
        """
        if paramstyle == "qmark":
            return "?"
        elif paramstyle == "numeric":
            return f":{int(key)}"
        elif paramstyle == "named":
            return f":{str(key)}"
        elif paramstyle == "format":
            return f"%s"
        elif paramstyle == "pyformat":
            return f"%({key})s"
        elif paramstyle is None:
            raise NotImplementedError("No paramstyle (placeholder format) for dialect")
        else:
            raise qbexception.APIError(
                f"Unknown paramstyle (placeholder format) '{paramstyle}' for dialect"
            )


class NullConnector(Connector):
    """A basic implementation of Connector, without backend

    This class is mostly aimed to debugging and testing.  It allows
    to visualize queries through the logger (activated by default),
    without executing anything on backend (in particular, SELECT
    queries will always return an empty relation).

    """

    class OperationalError(Exception):
        pass

    class NullDB:
        class NullCursor:
            def execute(self, query, args):
                pass

            def executemany(self, query, I):
                pass

            def __iter__(self):
                return iter(())

        def close(self):
            pass

        def cursor(self):
            return self.NullCursor()

        def commit(self):
            pass

        def rollback(self):
            pass

    def __init__(self, connect_info=None, logger=True, tokenizer=None, **kwargs):
        if tokenizer is None:
            tokenizer = querybuilder.settings["tokenizer"]
        super().__init__(connect_info, logger=logger, tokenizer=tokenizer, **kwargs)

    def connect(self):
        if not hasattr(self, "session"):
            self.session = self.NullDB()
            self.execute(
                self.queries.comment.SingleComment(
                    "Connected to Null (fake connector: nothing is executed)."
                )
            )
        return self.session
