from datetime import date, datetime, time, timedelta
from functools import singledispatchmethod, singledispatch
from itertools import chain
from typing import Any, Optional, Union, Iterable

from querybuilder.utils.decorators import instancemethod, typedispatch
from querybuilder.utils.typing import TkString, TkStream
import querybuilder.utils.constants as qbconstants
import querybuilder.utils.token as qbtoken
import querybuilder.utils.itertools as qbitertools
import querybuilder.classes.stores as stores
import querybuilder.queries.algebra.relations as relations
import querybuilder.queries.algebra.columns as qbcolumns
import querybuilder.queries.algebra.clauses as clauses
import querybuilder.queries.dql as dql
import querybuilder.queries.dml as dml
import querybuilder.queries.ddl as ddl
import querybuilder.queries.tcl as tcl
import querybuilder.queries.queries as queries
import querybuilder.queries.comment as comment
import querybuilder.schemas.constraints as constraints  # TODO: change this name for a more robust one (constraints might be in conflict with local variable names)


def name_and_defer(func):
    def f(tokenizer, *args, name=None, defer, **kwargs):
        if name:
            yield from tokenizer.tokenize.keyword(
                "CONSTRAINT", initialspace=True, endspace=True
            )
            yield from name

        yield from func(tokenizer, *args, **kwargs)

        yield from tokenizer.tokenize.keyword(defer)

    return f


class Supercast(object):
    __slots__ = ("instance", "type")

    def __init__(self, instance, type):
        self.instance = instance
        mro = instance.__class__.mro()
        self.type = mro[mro.index(type) + 1]


class Tokenizer:
    @instancemethod
    class tokenize:
        @staticmethod
        def __new__(cls, tokenizer, *args, **kwargs):
            # Tokenizer().tokenize(*args, **kwargs) is equivalent to Tokenizer()(*args, **kwargs)
            return tokenizer(*args, **kwargs)

        @classmethod
        def comment(cls, string="", /):
            yield qbtoken.Comment, f"-- {string}"

        @classmethod
        def name(cls, name: str):
            return qbitertools.join(
                (qbtoken.Token.Punctuation, "."),
                ((qbtoken.Token.Name, n) for n in name.split(".")),
            )

        @staticmethod
        def keyword(keyword, initialspace: bool = False, endspace: bool = False):
            flag = initialspace
            space = (qbtoken.Break, " ")
            for k in keyword.split(" "):
                if flag:
                    yield space
                else:
                    flag = True
                yield (qbtoken.Token.Keyword, k)
            if endspace:
                yield space

        @staticmethod
        def placeholder_key(
            key: Optional[str], anonymous_index: Optional[int] = None
        ) -> TkString:
            if key is None:
                if anonymous_index is None:
                    return ((qbtoken.Token.Name.Variable, "?"),)
                return ((qbtoken.Token.Name.Variable, f":auto{anonymous_index}"),)
            return ((qbtoken.Token.Name.Variable, f":{key}"),)

        @staticmethod
        def jointype(jointype: qbconstants.JoinType, natural: bool):
            space = qbtoken.Break(), " "
            if natural:
                yield qbtoken.Token.Keyword, "NATURAL"
                yield space

            yield qbtoken.Token.Keyword, jointype.value
            yield space
            yield qbtoken.Token.Keyword, "JOIN"

        @staticmethod
        def operator(operator):
            return ((qbtoken.Token.Keyword, operator),)

        @staticmethod
        def combinator(combinator) -> TkStream:
            return ((qbtoken.Token.Keyword, combinator),)

        @staticmethod
        def function(function):
            return ((qbtoken.Token.Name.Function, function),)

        @staticmethod
        def aggregator(aggregator):
            return ((qbtoken.Token.Name.Aggregator, aggregator),)

        @staticmethod
        @typedispatch
        def sqltype(type):
            if hasattr(type, "_sqltype"):
                return ((qbtoken.Token.Name.Builtin, type._sqltype),)
            raise ValueError(f"Unsupported type {type}")

        @sqltype.__func__.register(int)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "INTEGER"),)

        @sqltype.__func__.register(float)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "REAL"),)

        @sqltype.__func__.register(bool)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "BOOLEAN"),)

        @sqltype.__func__.register(str)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "TEXT"),)

        # TODO: following PostgreSQL here (https://www.postgresql.org/docs/current/datatype-datetime.html#DATATYPE-DATETIME-TABLE), check standard SQL
        @sqltype.__func__.register(datetime)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "TIMESTAMP"),)

        @sqltype.__func__.register(date)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "DATE"),)

        @sqltype.__func__.register(time)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "TIME"),)

        @sqltype.__func__.register(timedelta)
        def _(type):
            return ((qbtoken.Token.Name.Builtin, "INTERVAL"),)

        @staticmethod
        @typedispatch
        def constant(type: type, constant: Any) -> str:
            if hasattr(type, "_hard_encode"):
                return ((qbtoken.Token.Literal, type._hard_encode(constant)),)
            raise TypeError(type)

        @constant.__func__.register(int)
        @constant.__func__.register(float)
        def _(type, constant):
            return ((qbtoken.Token.Literal.Number, repr(type(constant))),)

        @constant.__func__.register(str)
        def _(type, constant):
            return ((qbtoken.Token.Literal.String, repr(type(constant))),)

        @constant.__func__.register(type(None))
        def _(type, constant):
            return ((qbtoken.Token.Keyword, "NULL"),)

        @constant.__func__.register(bool)
        def _(type, constant):
            return ((qbtoken.Token.Keyword, str(type(constant)).capitalize()),)

        @constant.__func__.register(datetime)
        @constant.__func__.register(date)
        @constant.__func__.register(time)
        @constant.__func__.register(timedelta)
        def _(type, constant):
            return ((qbtoken.Token.Literal.Date, str(constant)),)

        @staticmethod
        def value(value):
            l = [(qbtoken.Comment.Args.Arrow, "←")]
            if qbconstants.MISSING == value:
                l.append((qbtoken.Comment.Args.Value.MISSING, "ø"))
            else:
                l.append((qbtoken.Comment.Args.Value, repr(value)))
            return l

        @classmethod
        def values(cls, values):
            if values:
                yield qbtoken.Break(level=6), " "
                yield from cls.comment(
                    f"↖{values.to_valued_map(values.to_keyed_map(autokey=lambda i, j: j))!r}"
                )
                yield qbtoken.Break(level=6), " "

    def scope_it(self, tkstream, level=0, indent=0):
        yield (qbtoken.Token.Punctuation, "(")
        yield (qbtoken.Break(level=level, indent=indent), "")
        yield from tkstream
        yield (qbtoken.Break(level=level, indent=-indent), "")
        yield (qbtoken.Token.Punctuation, ")")

    @singledispatchmethod
    def __call__(self, obj, **kwargs):
        raise TypeError(obj)

    @__call__.register(Supercast)
    def _(self, obj: Supercast, **kwargs) -> TkStream:
        __call__ = self.__class__.__dict__["__call__"]

        for typ in obj.type.mro():
            func = __call__.dispatcher.registry.get(typ)
            if func:
                break
        else:
            func = __call__.func

        return func(self, obj.instance, **kwargs)

    ###############
    # DQL QUERIES #
    ###############

    @__call__.register(dql.Values)
    def _(
        self,
        obj,
        *,
        values: TkStream = (),  # TODO: Iterable[TkStream] ?
        orderby: TkStream = (),
        offset: TkStream = (),
        limit: TkStream = (),
    ):
        yield from self.tokenize.keyword("VALUES", endspace=True)
        sep = [(qbtoken.Token.Punctuation, ","), (qbtoken.Break(level=2), " ")]
        yield from chain.from_iterable(qbitertools.join(sep, values))
        yield from orderby
        yield from offset
        yield from limit

    @__call__.register(dql.Select)
    def _(
        self,
        obj,
        *,
        with_relations: tuple[TkStream, ...] = (),
        columns: tuple[TkStream, ...],
        distinct: TkStream = (),
        from_: TkStream = (),
        where: TkStream = (),
        groupby: TkStream = (),
        having: TkStream = (),
        orderby: TkStream = (),
        offset: TkStream = (),
        limit: TkStream = (),
    ):
        # assumed: each (but with_queries) given TkStream manages its initial space
        # TODO: manage indentation here

        yield qbtoken.Token.Keyword, "SELECT"
        yield from distinct
        yield from chain.from_iterable(
            qbitertools.join([(qbtoken.Token.Punctuation, ",")], columns)
        )
        yield from from_
        yield from where
        yield from groupby
        yield from having
        yield from orderby
        yield from offset
        yield from limit

    @__call__.register(dql.SetCombination)
    def _(self, obj, *, subrelations: tuple[TkStream, TkStream], combinator: TkStream):
        yield from subrelations[0]
        yield qbtoken.Break(level=3, chain=True), " "
        yield from combinator
        yield qbtoken.Break(level=3, chain=True), " "
        yield from subrelations[1]

    @__call__.register(clauses.DistinctColumn)
    def _(self, obj, *, column: Optional[TkStream] = None):
        yield qbtoken.Break, " "
        yield qbtoken.Token.Keyword, "DISTINCT"
        if column:
            yield qbtoken.Break, " "
            yield qbtoken.Token.Keyword, "ON"
            yield qbtoken.Break, " "
            yield qbtoken.Token.Punctuation, "("
            yield from column
            yield qbtoken.Token.Punctuation, ")"

    @__call__.register(clauses.WithClause)
    def _(self, obj, *, name: str, query: TkStream = (), materialized: bool):
        yield from self.tokenize.name(name)
        yield from self.tokenize.keyword("AS", initialspace=True, endspace=True)

        if materialized is False:
            yield from self.tokenize.keyword("NOT", endspace=True)
        if materialized is not None:
            yield from self.tokenize.keyword("MATERIALIZED")
            yield qbtoken.Break(level=2), " "

        yield qbtoken.Token.Punctuation, "("
        yield from query
        yield qbtoken.Token.Punctuation, ")"

    @__call__.register(dql.WithClosure)
    def _(self, obj, *, with_relations: tuple[TkStream, ...] = (), query: TkStream):
        if with_relations != ():
            yield from self.tokenize.keyword("WITH")
            yield qbtoken.Break(level=2, indent=+1, chain=True), " "
            sep = [
                (qbtoken.Token.Punctuation, ","),
                (qbtoken.Break(level=3, chain=True), " "),
            ]
            yield from chain.from_iterable(qbitertools.join(sep, with_relations))

            yield qbtoken.Break(level=2, indent=-1, chain=True), " "

        yield from query

    @__call__.register(clauses.AliasedColumn)
    def _(self, obj, *, column, alias: Optional[str] = None):
        yield qbtoken.Break(level=1, chain=True), " "
        yield from column

        if alias is not None:
            yield from self.tokenize.keyword("AS", initialspace=True, endspace=True)
            yield from alias

    @__call__.register(clauses.RelationClauseWrapper)
    def _(self, obj, *, relation):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("FROM", endspace=True)
        yield from relation

    @__call__.register(clauses.WhereColumn)
    def _(self, obj, *, column: TkStream):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("WHERE", endspace=True)
        yield from column

    @__call__.register(clauses.GroupColumn)
    def _(self, obj, *, column: TkStream):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("GROUP BY", endspace=True)
        yield from column

    @__call__.register(clauses.HavingColumn)
    def _(self, obj, *, column: TkStream):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("HAVING", endspace=True)
        yield from column

    @__call__.register(clauses.OrderColumn)
    def _(self, obj, *, column: TkStream, how: Optional[bool] = None):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("ORDER BY", endspace=True)
        yield from column

        if how:
            yield from self.tokenize.keyword("ASC", initialspace=True)
        elif how is False:
            yield from self.tokenize.keyword("DESC", initialspace=True)

    @__call__.register(clauses.Offset)
    def _(self, obj, *, offset: int):
        yield from self.tokenize.keyword("OFFSET", initialspace=True, endspace=True)
        yield from self.tokenize.constant(int, offset)

    @__call__.register(clauses.Limit)
    def _(self, obj, *, limit: int, with_ties: Optional[bool] = None):
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("FETCH FIRST", endspace=True)
        yield from self.tokenize.constant(int, limit)
        yield from self.tokenize.keyword(
            f"ROW{limit is True and '' or 'S'}", initialspace=True
        )
        if with_ties:
            yield from self.tokenize.keyword("WITH TIES", initialspace=True)
        elif with_ties is False:
            yield from self.tokenize.keyword("ONLY", initialspace=True)

    @__call__.register(clauses.SetCombinatorWrapper)
    def _(self, obj, *, combinator: qbconstants.SetCombinator, all: Optional[bool]):
        comb = f"{combinator.value}"
        if all:
            comb += " ALL"
        elif all is False:
            comb += " DISTINCT"

        return self.tokenize.keyword(comb)

    @__call__.register(clauses.TemporaryWrapper)
    def _(self, obj):
        yield from self.tokenize.keyword("TEMPORARY", initialspace=True)

    @__call__.register(clauses.IfNotExistsWrapper)
    def _(self, obj):
        yield from self.tokenize.keyword("IF NOT EXISTS", initialspace=True)

    @__call__.register(clauses.AsQueryWrapper)
    def _(self, obj, *, subquery: TkStream):
        yield from self.tokenize.keyword("AS", initialspace=True, endspace=True)
        yield from subquery

    ###############
    # DML QUERIES #
    ###############

    @__call__.register(dml.Insert)
    def _(
        self, obj, *, schema_relation: TkStream, query: TkStream, in_columns
    ):  # TODO: type of in_columns
        yield from self.tokenize.keyword("INSERT INTO", endspace=True)
        yield from schema_relation

        if in_columns:
            yield qbtoken.Token.Punctuation, "("
            yield qbtoken.Break(level=1, indent=+1, chain=True), ""
            sep = (qbtoken.Token.Punctuation, ","), (
                qbtoken.Break(level=0, chain=True),
                " ",
            )
            cols = ((qbtoken.Token.Name, c.name) for c in in_columns)
            yield from qbitertools.join(sep, cols, yieldfromsep=True)
            yield qbtoken.Break(level=1, indent=-1, chain=True), ""
            yield qbtoken.Token.Punctuation, ")"

        yield qbtoken.Break(indent=+1, level=4), " "
        yield from query
        yield qbtoken.Break(indent=-1, level=0), ""

    @__call__.register(dml.Delete)
    def _(self, obj, *, schema_relation: TkStream, where: Optional[TkStream] = None):
        yield from self.tokenize.keyword("DELETE FROM", endspace=True)
        yield from schema_relation
        if where:
            yield from where

    @__call__.register(dml.Update)
    def _(self, obj, *, schema_relation, set_columns, where):
        yield qbtoken.Token.Keyword, "UPDATE"
        yield qbtoken.Break, " "
        yield from self.tokenize_name(unambiguous_name)
        yield qbtoken.Break(level=4, indent=+1), " "
        yield qbtoken.Token.Keyword, "SET"
        yield qbtoken.Break(level=1, indent=+1, chain=True), " "
        sep = (qbtoken.Token.Punctuation, ","), (qbtoken.Break(level=1), " ")
        setting = (c.eq(expr).subtokenize() for c, expr in set_columns.items())
        setting = qbitertools.join(sep, setting, yieldfromsep=False)
        yield from chain.from_iterable(setting)
        yield qbtoken.Break(level=1, indent=-1, chain=True), ""

        if where:
            yield qbtoken.Break(level=4), " "
            yield qbtoken.Token.Keyword, "WHERE"
            yield qbtoken.Break, " "
            yield from where.subtokenize(self)
            yield qbtoken.Break(level=0), ""

        yield qbtoken.Break(level=0, indent=-1, chain=True), ""

    ###############
    # DDL QUERIES #
    ###############

    @__call__.register(ddl.CreateTable)
    def _(
        self,
        obj,
        *,
        name: TkStream,
        as_query: TkStream = (),
        columns: tuple[TkStream, ...] = (),
        constraints: tuple[TkStream, ...] = (),
        if_not_exists: TkStream = (),
        temporary: TkStream = (),
    ):
        yield from self.tokenize.keyword("CREATE")
        yield from temporary
        yield from self.tokenize.keyword("TABLE", initialspace=True)
        yield from if_not_exists
        yield qbtoken.Break, " "
        yield from name

        if as_query:
            yield from as_query
        else:
            constraints = (*columns, *constraints)

        yield qbtoken.Punctuation, "("
        # TODO: set level of Break as this is a good place to start a new line
        yield from chain.from_iterable(
            qbitertools.join(
                [(qbtoken.Token.Punctuation(), ","), (qbtoken.Break, " ")], constraints
            )
        )
        yield qbtoken.Punctuation, ")"

    @__call__.register(clauses.TableColumnWrapper)
    def _(self, obj, *, name: str, sqltype: type, constraints: Iterable[TkStream]):
        yield from self.tokenize.name(name)
        yield qbtoken.Break, " "
        yield from self.tokenize.sqltype(sqltype)
        if constraints:
            yield from chain.from_iterable(constraints)

    ###############
    # TCL QUERIES #
    ###############

    def _transaction_characteristics(self, isolation_level, read_only):
        if isolation_level:
            yield from self.tokenize.keyword(
                f"ISOLATION LEVEL {isolation_level}", initialspace=True
            )
        if read_only is not None:
            yield from self.tokenize.keyword(
                f"READ {read_only and 'ONLY' or 'WRITE'}", initialspace=True
            )

    @__call__.register(tcl.Initiate)
    def _(
        self,
        obj,
        *,
        action: str,
        isolation_level: Optional[str] = None,
        read_only: Optional[bool] = None,
    ):
        yield from self.tokenize.keyword(f"{action} TRANSACTION")
        yield from self._transaction_characteristics(isolation_level, read_only)

    @__call__.register(tcl.Conclude)
    def _(self, obj, *, action: str, chain: Optional[bool] = None):
        yield from self.tokenize.keyword(f"{action} WORK")
        if chain is not None:
            yield from self.tokenize.keyword(
                f"AND{chain and ' ' or ' NO '}CHAIN", initialspace=True
            )

    @__call__.register(tcl.ManageSavepoint)
    def _(self, obj, *, action: str, savepoint: str):
        if action == "CREATE":
            yield from self.tokenize.keyword("SAVEPOINT", endspace=True)
        elif action == "ROLLBACK":
            yield from self.tokenize.keyword(f"{action} TO SAVEPOINT", endspace=True)
        else:
            yield from self.tokenize.keyword(f"{action} SAVEPOINT", endspace=True)
        yield from self.tokenize.name(savepoint)

    ############
    # COMMENTS #
    ############

    @__call__.register(comment.SingleComment)
    def _(self, obj, *, message: str):
        yield qbtoken.Break(level=8, breakline=True), ""
        for msg in message.split("\n"):
            yield qbtoken.Comment.Single, "-- "
            yield qbtoken.Comment.Single, msg
            yield qbtoken.Break(level=0, breakline=True), "\n"

    @__call__.register(comment.MultilineComment)
    def _(self, obj, *, messages: tuple[str, ...]):
        yield qbtoken.Break(level=2), ""
        yield qbtoken.Comment.Multiline, "/*"
        yield qbtoken.Break(level=1, indent=+1, chain=True), " "
        flag = True
        for message in messages:
            if flag:
                flag = False
            else:
                yield qbtoken.Break(level=0), "\n"
            yield qbtoken.Comment.Multiline, message
        yield qbtoken.Break(level=1, indent=-1, chain=True), " "
        yield qbtoken.Comment.Multiline, "*/"

    ###############
    # CONSTRAINTS #
    ###############

    # @__call__.register(clauses.ConstraintNameWrapper)
    # def _(self, obj, *, name: str = None, **kwargs):
    #    if name:
    #        yield from self.tokenize.keyword("CONSTRAINT")
    #        yield qbtoken.Break, " "
    #        yield from name

    @__call__.register(constraints.Constraint)
    @name_and_defer
    def _(self, obj, **kwargs):
        # TODO: temporary rule during dev
        yield from self.tokenize.keyword(str(type(obj)))

    @__call__.register(constraints.ColumnPrimaryKey)
    @name_and_defer
    def _(self, obj):
        yield from self.tokenize.keyword("PRIMARY KEY", initialspace=True)

    @__call__.register(constraints.ColumnNotNull)
    @name_and_defer
    def _(self, obj):
        yield from self.tokenize.keyword("NOT NULL", initialspace=True)

    @__call__.register(constraints.ColumnUnique)
    @name_and_defer
    def _(self, obj):
        yield from self.tokenize.keyword("UNIQUE", initialspace=True)

    @__call__.register(constraints.ColumnGeneratedAsIdentity)
    @name_and_defer
    def _(self, obj, *, always: bool):
        yield from self.tokenize.keyword(
            f"GENERATED {always and 'ALWAYS' or 'BY DEFAULT'} AS IDENTITY",
            initialspace=True,
        )

    @__call__.register(constraints.ColumnGeneratedAlwaysAs)
    @name_and_defer
    def _(self, obj, *, expression: TkStream):
        yield from self.tokenize.keyword(
            "GENERATED ALWAYS AS", initialspace=True, endspace=True
        )

        yield from expression

        yield from self.tokenize.keyword("STORED", initialspace=True, endspace=True)

    @__call__.register(constraints.ColumnReferences)
    @name_and_defer
    def _(
        self,
        obj,
        *,
        table: str,
        colname: str,
        ondelete: TkStream = (),
        onupdate: TkStream = (),
    ):
        yield from self.tokenize.keyword("REFERENCES", initialspace=True, endspace=True)

        yield from self.tokenize.name(table)
        yield qbtoken.Punctuation, "("
        yield from self.tokenize.name(colname)
        yield qbtoken.Punctuation, ")"

        yield from ondelete
        yield from onupdate

    @__call__.register(constraints.ColumnDefault)
    @name_and_defer
    def _(self, obj, *, expression: TkStream):
        # TODO: fix double initial space when constraint isn't named
        yield from self.tokenize.keyword("DEFAULT", initialspace=True, endspace=True)
        yield from expression

    @__call__.register(constraints.TableUnique)
    @name_and_defer
    def _(self, obj, *, expression: TkStream):
        # TODO: fix double initial space when constraint isn't named
        yield from self.tokenize.keyword("UNIQUE", initialspace=True)

        yield from expression

    @__call__.register(constraints.TablePrimaryKey)
    @name_and_defer
    def _(self, obj, *, expression: TkStream):
        # TODO: fix double initial space when constraint isn't named
        yield from self.tokenize.keyword("PRIMARY KEY", initialspace=True)

        yield from expression

    @__call__.register(constraints.ColumnCheck)
    @__call__.register(constraints.TableCheck)
    @name_and_defer
    def _(self, obj, *, expression: TkStream):
        # TODO: fix double initial space when constraint isn't named
        yield from self.tokenize.keyword("CHECK", initialspace=True)

        yield qbtoken.Punctuation, "("
        yield from expression
        yield qbtoken.Punctuation, ")"

    @__call__.register(constraints.TableForeignKey)
    @name_and_defer
    def _(
        self,
        obj,
        *,
        table: str,
        expression: TkStream,
        refcolumns: TkStream,
        ondelete: TkStream = (),
        onupdate: TkStream = (),
    ):
        yield from self.tokenize.keyword(
            "FOREIGN KEY", initialspace=True, endspace=True
        )

        yield from expression

        yield from self.tokenize.keyword("REFERENCES", initialspace=True, endspace=True)

        yield from self.tokenize.name(table)
        yield from self.tokenize.name(refcolumns)

        yield from ondelete
        yield from onupdate

    @__call__.register(clauses.OnChangeWrapper)
    def _(self, obj, *, change: str, policy: constraints.OnChangePolicy):
        yield from self.tokenize.keyword(
            f"ON {change} {policy.value}", initialspace=True
        )

    #############
    # RELATIONS #
    #############

    @__call__.register(relations.Named)
    def _(self, obj, *, name: TkStream):
        return name

    @__call__.register(relations.Aliased)
    def _(self, obj, *, subrelation: TkStream, name: TkStream):
        yield qbtoken.Token.Punctuation, "("
        yield from subrelation  # TODO: improve scoping?
        yield qbtoken.Token.Punctuation, ")"

        yield from self.tokenize.keyword("AS", initialspace=True, endspace=True)

        yield from name

    @__call__.register(relations.CartesianProduct)
    def _(self, obj, *, subrelations: tuple[TkStream, ...]):
        sep = (qbtoken.Token.Punctuation, ","), (
            qbtoken.Break(level=2, chain=True),
            " ",
        )
        return chain.from_iterable(qbitertools.join(sep, subrelations))

    @__call__.register(relations.Join)
    def _(
        self,
        obj,
        *,
        subrelations: tuple[TkStream, TkStream],
        jointype: TkStream,
        on: Optional[TkStream] = None,
        using: tuple[TkStream, ...] = (),
    ):
        assert not on or not using

        space = qbtoken.Break(level=1), " "
        yield from subrelations[0]
        yield qbtoken.Break(level=2), " "
        yield from jointype
        yield space
        yield from subrelations[1]

        if on:
            yield qbtoken.Break(level=1), " "
            yield qbtoken.Token.Keyword, "ON"
            yield space
            yield from on
        elif using:
            yield qbtoken.Break(level=2), " "
            yield qbtoken.Token.Keyword, "USING"
            yield space
            yield qbtoken.Token.Punctuation, "("
            sep = ((qbtoken.Token.Punctuation, ","), space)
            yield from chain.from_iterable(qbitertools.join(sep, using))
            yield qbtoken.Token.Punctuation, ")"

    ###########
    # COLUMNS #
    ###########

    @__call__.register(qbcolumns.Column)
    def _(self, obj):
        return iter(((qbtoken.Token.Error, "<unnamed column>"),))

    @__call__.register(qbcolumns.Named)
    def _(self, obj, *, name: TkStream):
        return name

    @__call__.register(qbcolumns.Pretuple)
    def _(self, obj, *, columns: tuple[TkStream, ...]):
        sep = (
            (qbtoken.Token.Punctuation, ","),
            (qbtoken.Break(level=0, chain=True), " "),
        )
        return chain.from_iterable(qbitertools.join(sep, columns))

    @__call__.register(qbcolumns.Tuple)
    def _(self, obj, *, columns: tuple[TkStream, ...]):
        yield (qbtoken.Token.Punctuation, "(")
        yield (qbtoken.Break(level=1, chain=True, indent=+1), "")

        # yield from self(Supercast(obj, qbcolumns.Tuple), columns=columns)
        sep = (
            (qbtoken.Token.Punctuation, ","),
            (qbtoken.Break(level=0, chain=True), " "),
        )
        yield from chain.from_iterable(qbitertools.join(sep, columns))
        #

        yield (qbtoken.Break(level=1, chain=True, indent=-1), "")
        yield (qbtoken.Token.Punctuation, ")")

    @__call__.register(qbcolumns.BooleanCombination)
    def _(self, obj, *, combinator: TkStream, columns: tuple[TkStream, TkStream]):
        sep = (
            (qbtoken.Break(level=1, chain=True), " "),
            *combinator,
            (qbtoken.Break(level=0, chain=True), " "),
        )
        yield qbtoken.Break(level=0, indent=+1), ""
        yield from qbitertools.chain.from_iterable(qbitertools.join(sep, columns))
        yield qbtoken.Break(level=0, indent=-1), ""

    @__call__.register(qbcolumns.Not)
    def _(self, obj, *, combinator: TkStream, columns: tuple[TkStream]):
        yield from combinator
        yield qbtoken.Break, " "
        yield from columns[0]

    @__call__.register(qbcolumns.Comparison)
    def _(self, obj, *, operator: TkStream, columns: tuple[TkStream, TkStream]):
        yield from columns[0]
        yield qbtoken.Break(chain=True), " "
        yield from operator
        yield qbtoken.Break(chain=True), " "
        yield from columns[1]

    @__call__.register(qbcolumns.InRelation)
    def _(
        self, obj, *, operator: TkStream, columns: tuple[TkStream], relation: TkStream
    ):
        yield from columns[0]
        yield qbtoken.Break(chain=True), " "
        yield from operator
        yield qbtoken.Break(chain=True), " "
        yield from relation

    @__call__.register(qbcolumns.ArithmeticOperation)
    def _(self, obj, *, operator: TkStream, columns: tuple[TkStream, ...]):
        yield from qbitertools.chain.from_iterable(qbitertools.join(operator, columns))

    @__call__.register(qbcolumns.Negate)
    def _(self, obj, *, operator: TkStream, columns: tuple[TkStream]):
        yield from operator
        yield from columns[0]

    @__call__.register(qbcolumns.Transform)
    def _(self, obj, *, transformator: TkStream, columns: tuple[TkStream]):
        yield from transformator
        yield (qbtoken.Token.Punctuation, "(")
        yield from columns
        yield (qbtoken.Token.Punctuation, ")")

    @__call__.register(qbcolumns.Cast)
    def _(
        self,
        obj,
        *,
        transformator: TkStream,
        sqltype: TkStream,
        columns: tuple[TkStream],
    ):
        yield from transformator
        yield qbtoken.Token.Punctuation, "("
        yield from columns[0]
        yield qbtoken.Break, " "
        yield qbtoken.Token.Keyword, "AS"
        yield qbtoken.Break, " "
        yield from sqltype
        yield qbtoken.Token.Punctuation, ")"

    @__call__.register(qbcolumns.Aggregate)
    def _(
        self,
        obj,
        *,
        aggregator: TkStream,
        columns: tuple[TkStream],
        distinct: Optional[TkStream] = None,
    ):
        yield from aggregator
        yield qbtoken.Token.Punctuation, "("
        yield qbtoken.Break(chain=True), ""
        if distinct:
            yield from distinct
            yield qbtoken.Break, " "
        yield from columns[0]
        yield qbtoken.Break(chain=True), ""
        yield qbtoken.Token.Punctuation, ")"

    @__call__.register(qbcolumns.Exists)
    def _(self, obj, *, query: TkStream):
        yield qbtoken.Token.Keyword, "EXISTS"
        yield qbtoken.Token.Punctuation, "("
        yield qbtoken.Break(level=4, indent=+1, chain=True), ""
        yield from query
        yield qbtoken.Break(level=4, indent=-1, chain=True), ""
        yield qbtoken.Token.Punctuation, ")"

    @__call__.register(qbcolumns.Placeholder)
    def _(self, obj, *, key: TkStream):
        return key

    @__call__.register(qbcolumns.Constant)
    def _(self, obj, *, constant: TkStream):
        return constant

    @__call__.register(qbcolumns.Default)
    def _(self, obj):
        return self.tokenize.keyword("DEFAULT")

    def _post(self, tokens: TkStream) -> TkStream:
        nokeytok = self.tokenize.placeholder_key(None)
        i = 0

        def key_placeholder(e):
            if e == nokeytok:
                e = self.tokenize.placeholder_key(None, anonymous_index=i)
                i += 1
            return e

        return map(key_placeholder, tokens)
