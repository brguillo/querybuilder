import re
import warnings
from functools import singledispatchmethod
from typing import Optional
from querybuilder.utils.decorators import instancemethod
import querybuilder.utils.token as qbtoken
import querybuilder.queries.algebra.clauses as qbclauses
import querybuilder.schemas.constraints as constraints  # TODO: change this name for a more robust one (constraints might be in conflict with local variable names)
import querybuilder.drivers.sql.tokenizer as qbsql
import querybuilder.queries as qbqueries


class Tokenizer(qbsql.Tokenizer):
    @instancemethod
    class tokenize(qbsql.Tokenizer.tokenize.__func__):
        @classmethod
        def name(cls, name):
            # TODO: is this a good hack?
            pattern = re.compile("^public.")
            warnings.warn(
                "Main schema is called 'main' rather than 'public' in sqlite;  renamed 'public' into 'main'"
            )
            pattern = re.compile("^public.")
            name = pattern.sub("main.", name)
            return super().name(name)

    @singledispatchmethod
    def __call__(self, *args, **kwargs):
        return super().__call__(*args, **kwargs)

    # DQL
    @__call__.register(qbclauses.Limit)
    def _(self, obj, /, limit: int, limit_with_ties: Optional[bool] = None):
        space = qbtoken.Break, " "
        yield qbtoken.Break(level=2), " "
        yield from self.tokenize.keyword("LIMIT")
        yield qbtoken.Break, " "
        yield from self.tokenize.constant(int, limit)
        if self.limit_with_ties:
            raise NotImplementedError()  # TODO: is this the correct exception to raise here?

    # TCL
    @__call__.register(qbqueries.tcl.Start)
    def _(
        self,
        obj,
        /,
        action: str,
        isolation_level: Optional[str] = None,
        read_only: Optional[bool] = None,
    ):
        yield from self.tokenize.keyword(f"BEGIN TRANSACTION")
        # TODO: ISOLATION LEVEL/READ ONLY are not available in sqlite.  Where to raise?
        yield from self._transaction_characteristics(isolation_level, read_only)

    @__call__.register(qbqueries.tcl.Conclude)
    def _(self, obj, *, action: str, chain: Optional[bool] = None):
        yield from self.tokenize.keyword(f"{action} TRANSACTION")
        # TODO: CHAIN are not available in sqlite.  Where to raise?
        if chain is not None:
            yield from self.tokenize.keyword(
                f"AND{chain and ' ' or ' NO '}CHAIN", initialspace=True
            )

    # DDL
    @__call__.register(constraints.ColumnGeneratedAsIdentity)
    @qbsql.name_and_defer
    def _(self, obj, *, always: bool):
        if always:
            warnings.warn(
                f"sqlite does not support GENERATED ALWAYS AS IDENTITY constraints"
            )
        # TODO: works only in case of integer column
        yield from self.tokenize.keyword("AUTOINCREMENT", initialspace=True)
