import sqlite3
import querybuilder.drivers.sql.connector as qbconn
import querybuilder.drivers.sqlite.tokenizer as qbsqlitetokenizer


class Connector(qbconn.Connector):
    __slots__ = ()
    OperationalError = sqlite3.OperationalError

    def connect(self):
        if not hasattr(self, "session"):
            self.session = sqlite3.connect(self._connect_info)
            # TODO: create a pseudo information_schema (script sql) -- see https://www.sqlite.org/pragma.html
        return self.session

    def _set_tokenizer(self, tokenizer=None):
        if tokenizer is None:
            tokenizer = qbsqlitetokenizer.Tokenizer()
        super()._set_tokenizer(tokenizer=tokenizer)

    @classmethod
    def get_placeholder_marker(cls, key):
        """sqlite placehoder marking

        Though sqlite3.paramstyle is 'qmark', the module accepts
        two other kinds of placeholder markers: 'numeric' and
        'named' (see comment below).  The function chooses one of
        these three kinds according to whether key is None, an int
        or a str.

        Actually, in the official documentation¹, one may find (in
        March 2022) that 'qmark' and 'named' are the only two
        accepted styles, and, later when documenting the paramstyle
        constant, that 'qmark' and 'numeric' are the only two
        accepted styles; it happens that the three styles are
        indeed accepted.

        Parameters
        ----------
        key : int or str or None
            the placeholder key if any, or None, otherwise.

        References
        ----------
        ¹ https://docs.python.org/3/library/sqlite3.html#sqlite3-placeholders
        """
        if isinstance(key, int):
            paramstyle = "numeric"
        elif isinstance(key, str):
            paramstyle = "named"
        else:
            # default to 'qmark' == 'sqlite3.paramstyle
            paramstyle = "qmark"
        return super().get_placeholder_marker(key, paramstyle=paramstyle)
