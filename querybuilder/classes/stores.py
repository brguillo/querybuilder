from __future__ import annotations
from functools import partial, singledispatchmethod
from itertools import chain
from typing import Any, Callable, ClassVar, Generic, Iterable, Optional
import querybuilder.classes.atoms as atoms
import querybuilder.utils.constants as qbconstants
from querybuilder.utils.typing import KeyedProto, NamedProto, E, K

import querybuilder

# from querybuilder.classes.atoms import Atom
# from querybuilder.algebra.columns import Placeholder
# import querybuilder.algebra.relations as relations
# from querybuilder.queries.queries import Withable


# Abstracts
class KeyedTuple(tuple[E, ...], Generic[E, K]):
    """Tuple whose elements can be accessed by attribute lookup when unambiguously associated with a string key

    The static method `_get_key` is expected to return a hashable key for each element
    of the tuple.  Whenever the returned key is a string `s` which uniquely identify the
    element, this element can be accessed as attribute `s` of the KeyedTuple.

    Parameters
    ----------
    iterable: Iterable[E], default=()
        the elements to gather within the KeyedTuple.

    Class attributes
    ----------------
    _get_key: Callable[[E], K]
        a staticmethod defining how a key is returned from an element.

    _key_str: str, default='key'
        the meta-name of the key, to be used in exception messages (e.g., for speaking
        of "ambiguous key").

    _prepare: Callable[Iterable[E], Iterable[E]]
        a hook that applies on the unique parameter passed to the class constructor, in
        order to filter accepted values.  A typical use is to define it in such a way
        that ambiguity is forbidden.  See concretes implementations below for examples.

    Examples
    --------
    >>> class AlphaT(KeyedTuple[list[str], str]):
    ...     _get_key = staticmethod(''.join)
    >>> t = AlphaT((['a', 'bc'], ['a'], ['ab', 'cd'], ['a']))

    Now, `t` is a tuple…
    >>> len(t)
    4
    >>> ['a', 'bc'] in t
    True
    >>> ['abc'] in t
    False
    >>> tuple(t)
    (['a', 'bc'], ['a'], ['ab', 'cd'], ['a'])
    >>> t[2]
    ['ab', 'cd']
    >>> t[-1]
    ['a']
    >>> t[2] is t[-1]
    False

    … but it is an AlphaT…
    >>> type(t).__qualname__
    'AlphaT'
    >>> repr(t)
    "AlphaT((['a', 'bc'], ['a'], ['ab', 'cd'], ['a']))"

    … this allows us to access its (list-of-strings) elements by the corresponding key
    (which is the concatenation of its elements in this example)…
    >>> t.abc
    ['a', 'bc']
    >>> t.abcd
    ['ab', 'cd']
    >>> t.b # no element is keyed by 'b'
    Traceback (most recent call last):
        ...
    AttributeError: b
    >>> l = t.abc
    >>> l.append('d')
    >>> l
    ['a', 'bc', 'd']
    >>> t.abc
    Traceback (most recent call last):
        ...
    AttributeError: abc
    >>> t.abcd # several distinct elements are keyed by 'abcd'
    Traceback (most recent call last):
        ...
    AttributeError: Ambiguous key 'abcd', got ['a', 'bc', 'd'] and ['ab', 'cd']
    >>> t.a # several elements are keyed by 'a', and they are all equal
    Traceback (most recent call last):
        ...
    AttributeError: Ambiguous key 'a', got ['a'] and ['a']

    KeyedTuple can be concatenated, by using the bit-or operator (`|`).
    >>> s0 = AlphaT((['a'], ['a', 'bc']))
    >>> s1 = AlphaT((['a'], ['d', 'e']))
    >>> s2 = s0 | s1
    >>> type(s2).__qualname__
    'AlphaT'
    >>> repr(s2)
    "AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))"
    >>> s2.abc
    ['a', 'bc']
    >>> s2.de
    ['d', 'e']

    Actually the right tuple does not need to be an instance of AlphaT.
    >>> s3 = s0 | (['a'], ['d', 'e'])
    >>> repr(s3)
    "AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))"

    It may even be an arbitrary iterable.
    >>> s0 | (e for e in s1)
    AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))

    If the left side object is an iterable that does not defined how to be or-ed with
    AlphaT instances, it is still possible to concatenate it with such an instance.
    >>> tuple(s0) | s1
    AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))

    Finally, you can get the set of elements keys through the `_keys` property.
    >>> type(s2._keys).__qualname__
    'frozenset'
    >>> sorted(s2._keys)
    ['a', 'abc', 'de']

    The unambiguous ones populate the dir list.
    >>> hasattr(s2, 'a')
    False
    >>> sorted(x for x in dir(s2) if not x.startswith('_'))
    ['abc', 'count', 'de', 'index']

    """

    __slots__ = ()
    _get_key: Callable[[E], K]
    _key_attr: ClassVar[str] = "key"

    @classmethod
    def _prepare(cls, iterable: Iterable[E]) -> Iterable[E]:
        return iterable

    @staticmethod
    def __new__(cls, iterable: Iterable[E] = (), /) -> KeyedTuple[E, K]:
        return super().__new__(cls, cls._prepare(iterable))

    def __getattr__(self, attr: str) -> E:
        found = None
        for obj in self:
            if self.__class__._get_key(obj) == attr:
                if found:
                    raise AttributeError(
                        f"Ambiguous {self._key_attr} {attr!r}, got {found!r} and {obj!r}"
                    )
                found = obj
        if found:
            return found
        raise AttributeError(attr)

    def __or__(self, other: Iterable[E]) -> KeyedTuple[E, K]:
        if not isinstance(other, Iterable):
            return NotImplemented
        elif not other:
            return self
        return self.__class__(chain(self, other))

    def __ror__(self, other: Iterable[E]) -> KeyedTuple[E, K]:
        # TODO: remove this method (useless?)
        if not isinstance(other, Iterable):
            return NotImplemented
        return self.__class__(chain(other, self))

    def __sub__(self, other: Iterable[E]) -> KeyedTuple[E, K]:
        if not isinstance(other, Iterable):
            return NotImplemented

        return self.__class__((e for e in self if e not in other))

    def __dir__(self) -> list[str]:
        d = list(super().__dir__())
        d.extend(n for n in self._keys if isinstance(n, str) and hasattr(self, n))
        return d

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({super().__repr__()})"

    @property
    def _keys(self) -> frozenset[K]:
        return frozenset(map(self._get_key, self))

    def _repr_pretty_(self, printer, cycle=False):
        if cycle:
            printer.text("…")
            return
        printer.text(f"{self.__class__.__qualname__}(")
        flag = False
        for e in self:
            flag = flag and printer.text(", ") or True
            self._element_repr_pretty_(e, printer)
        printer.text(")")

    @staticmethod
    def _element_repr_pretty_(element, printer):
        if hasattr(element, "_pretty"):
            printer.text(element._pretty())
        else:
            printer.pretty(element)


# Concretes
class NamedStore(KeyedTuple[NamedProto, str]):
    """KeyedTuple that uses the 'name' attribute as key"""

    __slots__ = ()
    _key_attr = "name"

    @staticmethod
    def _get_key(e: NamedProto, /) -> str:
        return e.name


class UNamedStore(NamedStore):
    """NamedStore that does not allow two named elements to share the same name

    Such stores are mostly used in schema objects such as Schema, Table or TableColumn.
    """

    __slots__ = ()

    @classmethod
    def _prepare(cls, iterable: Iterable[E] = (), /) -> Iterable[E]:
        seen: dict[K, E] = {}

        def fltr(e: E) -> bool:
            k = cls._get_key(e)
            if k is None:
                return True
            prev = seen.get(k)
            if prev is None:
                seen[k] = e
                return True
            raise ValueError(f"Ambiguous {cls._key_attr} {k!r}, got {prev!r} and {e!r}")

        return filter(fltr, iterable)
