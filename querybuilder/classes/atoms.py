import functools
from itertools import chain
import querybuilder


class Atom(object):
    __slots__ = ()
    _scopable = False

    @classmethod
    def __init_subclass__(cls, **kwargs):
        if "__slots__" in cls.__dict__ and "__getstate__" not in cls.__dict__:

            def __getstate__(self):
                return super(cls, self).__getstate__() | {
                    k: getattr(self, k) for k in cls.__slots__
                }

            setattr(cls, "__getstate__", __getstate__)

    def accept(self, accumulator):
        """Accepts an accumulator that will visit this object in a bottom-up manner."""

        return accumulator.visit(self)

    def descend(self, accumulator):
        """Descends in this object in a top-down manner with an accumulator."""
        return accumulator.visit(self)

    def get_free_with(self):
        """Gets the free queries that appear in this object.

        Returns
        -------
            a WithQueryStore that contains the free with queries of this object
        """
        return self.accept(querybuilder.visitors.stores.WithQueryStore())

    def get_placeholders(self):
        return self.descend(querybuilder.visitors.stores.PlaceholderStore())

    def substitute(self, substitutions):
        return substitutions.get(self, self)

    def __getstate__(self):
        return {}

    def __setstate__(self, state):
        for k, v in state.items():
            setattr(self, k, v)

    @classmethod
    def buildfrom(cls, *instances, **kwargs):
        state = {}

        for inst in instances:
            state.update(inst.__getstate__())

        state.update(kwargs)
        self = object.__new__(cls)
        self.__setstate__(state)

        return self

    def __eq__(self, other):
        return (
            self.__class__ == other.__class__
            and self.__getstate__() == other.__getstate__()
        )

    def __hash__(self):
        return hash((self.__class__, *sorted(self.__getstate__().items())))

    def _get_subtokenize_kwargs(self, tokenizer):
        return {}

    def _get_slot_values(self):
        slots = chain.from_iterable(
            getattr(k, "__slots__", []) for k in self.__class__.mro()
        )
        return {k: getattr(self, k) for k in slots}

    def subtokenize(self, tokenizer, scoped=False):
        if scoped and self._scopable:
            return tokenizer.scope_it(self.subtokenize(tokenizer))
        return tokenizer(self, **self._get_subtokenize_kwargs(tokenizer))

    def tokenize(self, tokenizer):
        return tokenizer._post(self.subtokenize(tokenizer))

    def _pretty_subtokenize(self, tokenizer):
        yield from self.subtokenize(tokenizer)
        yield from tokenizer.tokenize.values(self.get_placeholders())

    def _pretty(self):
        tokenizer = querybuilder.settings["tokenizer"]
        formatter = querybuilder.settings["pretty_formatter"]
        tokens = self._pretty_subtokenize(tokenizer)
        formatted = formatter.get_formatted(tokens)

        return formatted

    def __str__(self):
        return "".join(t[1] for t in self.tokenize(querybuilder.settings["tokenizer"]))

    def pretty_print(self, **kwargs):
        print(self._pretty(), **kwargs)

    def _repr_pretty_(self, printer, cycle):
        printer.text(f"{self.__class__.__name__}: ")
        if cycle:
            printer.text("…")
            return
        printer.text(self._pretty())
