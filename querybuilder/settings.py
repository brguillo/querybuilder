from collections import ChainMap
import querybuilder.utils.formatter as formatter
import querybuilder.drivers.sql.tokenizer as tokenizer

settings = ChainMap(
    {},
    dict(
        raw_formatter=formatter.Formatter(),
        pretty_formatter=formatter.PrettyFormatter(),
        log_formatter=formatter.PrettyFormatter(),
        tokenizer=tokenizer.Tokenizer(),
    ),
)
if formatter.with_pygments:
    settings.maps[1][
        "pretty_styles"
    ] = formatter.PrettyFormatter.default_subformatter_kwargs["style"].styles
