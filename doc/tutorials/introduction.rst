Introduction
============

The goal of this module is to provide a flexible and complete way of manipulating SQL queries
and logical objects (e.g., tables, views) abstractly as Python objects in a composable way.


.. doctest::
	:options: +NORMALIZE_WHITESPACE

	>>> import functools, sys, types
	>>> import termcolor
	>>> import pygments
	>>> import querybuilder as qb
	>>> 
	>>> dial = qb.sqlite
	>>> db = dial.schema.db
	>>> sch = db.add_schema("main", ifexists="replace")
	>>>
	>>> nodes = sch.add_table("nodes")
	>>> nid = nodes.add_column("id", "integer")
	>>> nid.add_primarykey()
	>>> nid.add_generated_as_identity()
	>>> nname = nodes.add_column("name", "text")
	>>> nname.add_notnull()
	>>> 
	>>> node_data = sch.add_table("node_data")
	>>> ndid = node_data.add_column("id", "integer")
	>>> ndid.add_primarykey()
	>>> ndid.add_generated_as_identity()
	>>> ndfid = node_data.add_column("fid", "integer")
	>>> ndfid.add_references(nid, deferrable=True, initially_deferred=True)
	>>> ndfid.add_notnull()
	>>> ndkey = node_data.add_column("key", "text")
	>>> ndkey.add_notnull()
	>>> ndval = node_data.add_column("value", "text")
	>>> ndval.add_notnull()
	>>> node_data.add_unique(ndfid.tuple_with(ndkey))
	>>> 
	>>> 
	>>> edges = sch.add_table("edges")
	>>> eid = edges.add_column("id", "integer")
	>>> eid.add_primarykey()
	>>> eid.add_generated_as_identity(always=True)
	>>> esrc = edges.add_column("source", "integer")
	>>> esrc.add_references(nid, deferrable=True, initially_deferred=True)
	>>> esrc.add_notnull()
	>>> etrgt = edges.add_column("target", "integer")
	>>> etrgt.add_references(nid, deferrable=True, initially_deferred=True)
	>>> etrgt.add_notnull()
	>>> edges.add_unique(esrc.tuple_with(etrgt))
	>>> 
	>>> edge_data = sch.add_table("edge_data")
	>>> edid = edge_data.add_column("id", "integer")
	>>> edid.add_primarykey()
	>>> edid.add_generated_as_identity(always=True)
	>>> edfid = edge_data.add_column("edge", "integer")
	>>> edfid.add_references(eid, deferrable=True, initially_deferred=True)
	>>> edfid.add_notnull()
	>>> edkey = edge_data.add_column("key", "text")
	>>> edkey.add_notnull()
	>>> edval = edge_data.add_column("value", "text")
	>>> edval.add_notnull()
	>>> edge_data.add_unique(edfid.tuple_with(edkey))
	>>> 
	>>> 
	>>> graph_data = sch.add_table("graph_data")
	>>> gdkey = graph_data.add_column("key", "text")
	>>> gdkey.add_primarykey()
	>>> gdval = graph_data.add_column("value", "text")
	>>> gdval.add_notnull()
	>>> 
	>>> 
	>>> #stores
	>>> nodestore = nodes.join("left", node_data, nid.eq(ndfid))
	>>> edgestore = edges.join("left", edge_data, on=eid.eq(edfid))
	>>> graphstore = graph_data.to_relation()
	>>> succstore = nodes.join("left", edges, on=nid.eq(esrc)).join("left", edge_data, on=eid.eq(edfid))
	>>> predstore = nodes.join("left", edges, on=nid.eq(etrgt)).join("left", edge_data, on=eid.eq(edfid))
	>>> 
	>>> 
	>>> #styles for printing
	>>> styles = {
	... 	qb.sql.schema.DB: "magenta",
	... 	qb.sql.schema.Schema: "green",
	... 	qb.sql.schema.Relation: "red",
	... 	qb.sql.schema.TableColumn: "cyan",
	... 	qb.sql.constraints.Object: "yellow"
	>>> }
	>>> styles = { k: functools.partial(termcolor.colored, color=v) for k, v in styles.items() }
	>>> 
	>>> class Formatter:
	... 	class _default_file:
	... 		"""A class with a write method
	...
	... 		The format method of pygments formatters require an opened file into
	... 		which to write the formatted strings.  In order to return the string
	... 		instead of writing it to a file, we may use this naive class, that
	... 		mimics the write method, but store the result in the string instance
	... 		attribute.
	... 		"""
	... 		def __init__(self):
	... 			self.written_string = ''
	... 		def __enter__(self):
	... 			self.written_string = ''
	... 			return self
	... 		def __exit__(self, exc, args, tb):
	... 			pass
	... 		def write(self, string):
	... 			self.written_string += string
	... 		def flush(self):
	... 			pass
	... 		def __str__(self):
	... 			return self.written_string
	... 	class _default_style(pygments.styles.default.DefaultStyle):
	... 		default_style = pygments.styles.default.DefaultStyle.default_style
	... 		styles = dict(pygments.styles.default.DefaultStyle.styles)
	... 		styles.update({
	... 			pygments.token.Keyword: 'bold #3344cc',
	... 			pygments.token.Comment: 'italic #999999',
	... 			pygments.token.Name: '#11bb00',
	... 			pygments.token.Name.Function: '#00ff00',
	... 			pygments.token.Punctuation: '#7777cc',
	... 			pygments.token.Operator: '#999999',
	... 			pygments.token.Token.OutPrompt: '#999999',
	... 			pygments.token.Comment.Args: 'noitalic #999999',
	... 		})
	... 	_default_formatter = pygments.formatters.Terminal256Formatter
	... 	_default_formatter_kwargs = dict()
	... 	def __init__(self,formatter=_default_formatter, style=_default_style, file=_default_file(), **formatter_kwargs):
	... 		self.formatter = formatter(style=style)
	... 		self.file = file
	... 		self.formatter_kwargs = dict(self._default_formatter_kwargs, **formatter_kwargs)
	... 	def format(self, tokens, **kwargs):
	... 		tokens = qb.token.breakinglines(tokens, **dict(self.formatter_kwargs, **kwargs))
	... 		if hasattr(self.formatter, 'format'):
	... 			if hasattr(self.file, '__enter__'):
	... 				with self.file as f:
	... 					self.formatter.format(tokens, f)
	... 			elif hasattr(self.file, 'write'):
	... 				self.formatter.format(tokens, f)
	... 			elif isinstance(self.file, str):
	... 				with open(self.file, 'a') as f:
	... 					self.formatter.format(tokens, f)
	... 			elif callable(self.file):
	... 				f = self.file()
	... 				self.formatter.format(tokens, f)
	... 			res = getattr(f, 'written_string', None)
	... 		else:
	... 			res = ''.join(map(lambda s: s[1], tokens))
	... 		return res
	>>> qb.sql.bases.SQLObject._formatter = Formatter(maxlen=96, lineprefix='···〉', initiallineprefix='SQL〉')
	>>> #qb.sql.bases.SQLObject._pretty_repr_ = lambda self: self._subrepr(**self._pretty_repr_kwargs) if self._subrepr() else self._subrepr() #avoid printing type
	>>> 
	>>> #printing
	>>> 
	>>> connector = dial.connector.Connector(":memory:", logger=dict(formatter=qb.sql.bases.SQLObject._formatter))
	>>> with connector.transaction(oncontext=0) as t:
	... 	t.execute(dial.queries.pragmas.Setter('foreign_keys', dial.columns.true))
	>>> with connector.transaction as t:
	... 	t.execute(nodes.create(ifnotexists=True))
	... 	t.execute(node_data.create(ifnotexists=True))
	... 	t.execute(edges.create(ifnotexists=True))
	... 	t.execute(edge_data.create(ifnotexists=True))
	... 	t.execute(graph_data.create(ifnotexists=True))
	>>> 
	>>> print(db.tree_repr(styles=styles))
	>>> nodestore.prettyprint()
	>>> edgestore.prettyprint()
	>>> graphstore.prettyprint()
	>>> succstore.prettyprint()
	>>> predstore.prettyprint()
	>>> print('-'*96)
