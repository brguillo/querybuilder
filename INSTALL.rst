Install
=======

NetworkDisk requires Python3.6.

Below we assume you have the default Python environment already configured on
your computer and you intend to install ``networkx`` inside of it.  If you want
to create and work with Python virtual environments, please follow instructions
on `venv <https://docs.python.org/3/library/venv.html>`_ and `virtual
environments <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_.

First, make sure you have the latest version of ``pip`` (the Python package manager)
installed. If you do not, refer to the `Pip documentation
<https://pip.pypa.io/en/stable/installing/>`_ and install ``pip`` first.

Installing with dependencies
----------------------------

To install ``QueryBuilder`` with dependencies from source, run the following command from the package root directory::

    $ python3 -m pip install .

To install optional dependencies to run tests or generate documentation, run the following commands from the package root directory::

    $ python3 -m pip install .[test]
    $ python3 -m pip install .[doc]
