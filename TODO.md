# Design
+ why are context added in subtokenize methods (shouldn't it be the case that the given context is already good?)? 
+ we might handle anonymous named relations as unkeyed placeholders, using context and, perhaps, get_named_relations method

# Structure
+ move SQL base class (bases, columns, relations, as well as maybe schema, constraints, and parts of types and dialect) from qb.drivers.sql to qb.drivers, in order to keep language specific objects only in submodules.

# Connectors
+ (!) test sqlite connector
+ (!) include very nice logger from nodestore.py, if pygments is installed and output is terminal
+ (!) develop PostgreSQL connector and test
+ develop Oracle connector and test

# Schema
+ (!) where to place indexes in the tree structure of databases: as children of schema or as children of relations?
	1. as children of schema because there name are supposed to be unambiguous at that level
	2. as children of relations because they apply on the relation
+ Auto-naming of indexes?  of constraints?

# Queries
+ ddl and schema
+ More TCL queries (isolation level…)
+ Alter queries
+ Drop many same-kind schema objects at once
+ propagate keyworded parameters to schema object methods
+ (!) view creation, table as creation, trigger, index

# Doc and tests
+ (!) add many doctests

----
(!) short-term TODO for networkdisk
